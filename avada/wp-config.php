<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'avada');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'SxpXkfX`}m9iG6:n-:PTtQJY*nl?UoQHh9)6()pWrjI-p7DMX;-#CGdcxfoj}f0}');
define('SECURE_AUTH_KEY',  'aHjhZBQ[rRx&@Ltb?-w PNK2%+mPvK b<+aCY{+~:.AWr>[KLBwE~_p1az|6t*b;');
define('LOGGED_IN_KEY',    ',$AO_l~UQSKU<aU:hfDVf|qI.Xl}V;U;A}7,I$z0wg?DN,}3db:=?Y+I4OIr4+kt');
define('NONCE_KEY',        'yC0lOjHrx3TG-}{n(R-dm*LMUPD;mt++hMkmPB/6`^RI_ew`<~)i<Ob]jCmp9NH)');
define('AUTH_SALT',        'Fl i}61[||![ieud$HGvVTfm(ce--UD#)bU.zOOZh^Y|r{E?[]~eO)4~h`Y$v,3t');
define('SECURE_AUTH_SALT', '#g,Pe}&?IG}RTo|5x2?0)bVx1gTj@U;e<W41b1~&YYZ*jG:W|rHPsbT1uwM$wSF|');
define('LOGGED_IN_SALT',   'G9HazaVrh-C4!V;Fb-LQ, F0jux[BMum9u(:;ud-SCgSPr+70E.>-ln!$BjjYZ`+');
define('NONCE_SALT',       'R+nEHr&e+2rwQiXwAz8|{]bK@(fpC>L5nOTayjX^<:=nT@2Iu@ZLE[$gS5KWc%V|');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
