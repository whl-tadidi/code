<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'paradisehotel');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '!>J0+g|$Ft|<T5StIWwY/~;$<jy`eA$9C5]E_z-eHKhCQp7th{[+r!v~y^7K-nro');
define('SECURE_AUTH_KEY',  'F@>(Bh]|e+`H9x$1XwQm?Mm5)A~^Sg7-04+m:oMevbFegv1utqfBU 7<~plq9/BD');
define('LOGGED_IN_KEY',    'EyTgA+Ul=gN`t- 26}G(-yg_gAjR.F(}~wT Jwm%/,00g8ljY*Xf2{Q=Kq4>Y9-a');
define('NONCE_KEY',        '5t@yU*OA,kQs9`oye.PG28:m~[g(kS2n}9H$S?(nO9-p%{aJV+2;<(z 0;{-zv@z');
define('AUTH_SALT',        'N1HuE{[_Q1^2<w>|>}kY-[b_ZlpkCl*^|+K8b2-NW_pK>m4i`s&XqlE*&d0.GH+N');
define('SECURE_AUTH_SALT', '|UHSpa^T|#Km)uHMY$uGSe8{JDdn,/g1_(%`nm_t-koiW8++YFZ3:i#Vc=&6>+Z&');
define('LOGGED_IN_SALT',   'N!kgZfj^O`ez2%ou-?5LN$aXA45uQ|*; $b/L2{sf)+!Sj%f.b}gGN%?dC#GEWy;');
define('NONCE_SALT',       '?2qlMPK!(|XBq9ck-,%/HlSXY/vHkI@hwc=-|ZvxH< A*1+N%e:%qaHwdhPJ{6H.');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
