jQuery(document).ready(function() {
    
    var FS = window.FS || {} ;
    
    // settings for flex slider
    if(typeof(FS.animationLoop)!='undefined'){
        FS.animationLoop = FS.animationLoop =='false' ? false : true;
    }
    
    if(typeof(FS.slideshow)!='undefined'){
        FS.slideshow = FS.slideshow =='false' ? false : true;
    }
    
    if(typeof(FS.pauseOnAction)!='undefined'){
        FS.pauseOnAction = FS.pauseOnAction =='false' ? false : true;
    }
    
    if(typeof(FS.pauseOnHover)!='undefined'){
        FS.pauseOnHover = FS.pauseOnHover =='false' ? false : true;
    }
    
    if(typeof(FS.controlNav)!='undefined'){
        FS.controlNav = FS.controlNav =='false' ? false : true;
    }
    
     if(typeof(FS.randomize)!='undefined'){
        FS.randomize = FS.randomize =='false' ? false : true;
    }
    
     if(typeof(FS.directionNav)!='undefined'){
       // FS.directionNav = FS.directionNav =='false' ? false : true;
       
       
    }
    
   FS.directionNav =  false;
    
    
   // console.log(FS);
    
    
    jQuery('.flexslider').flexslider(FS);
    
    
    jQuery("body").fitVids();
    
    jQuery(".validate-form").validate();
    jQuery.extend(jQuery.validator.messages,window.validateMsg);
	jQuery(".date-input").dateinput({

    });
	
	
	// load video thumbnail 
    jQuery('.video-thumb').each(function(){
        var obj = jQuery(this);
        var v = obj.attr('video');
        var vi = obj.attr('video-id');
        if(typeof(v)!='undefined' && v !=''&& typeof(vi)!='undefined' && vi !=''){
            if(v=='youtube'){
                obj.html('<img src="http://img.youtube.com/vi/'+vi+'/3.jpg" alt="" />');
            }else{
                 jQuery.getJSON('http://vimeo.com/api/v2/video/'+vi+'.json?callback=?',{format:"json"},function(data,status){
                    var small_thumb=  data[0].thumbnail_small;
                    obj.html('<img src="'+small_thumb+'" alt="" />');
                });
            }
        }
    });
	
	
	/* GOOGLE MAP ======================================== */
    if(typeof google.maps.LatLng !== 'undefined'){
        jQuery('.map_canvas').each(function(){
            
            var $canvas = jQuery(this);
            var dataZoom = $canvas.attr('data-zoom') ? parseInt($canvas.attr('data-zoom')) : 10;
            
            var latlng = $canvas.attr('data-lat') ? 
                            new google.maps.LatLng($canvas.attr('data-lat'), $canvas.attr('data-lng')) :
                            new google.maps.LatLng(40.7143528, -74.0059731);
                            
            var mapID=  $canvas.attr('map-id');   
            
          
            var myOptions = {
                zoom: dataZoom,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                center: latlng
            };
                    
            var map = new google.maps.Map(document.getElementById(mapID), myOptions);
            
            
          
                            
            var marker = new google.maps.Marker({
                position: latlng,
                title: $canvas.attr('data-maptitle')
            });
                    
                            
            marker.setMap(map);                
            var infowindow = new google.maps.InfoWindow({
                  content:"<b>"+$canvas.attr('data-maptitle')+'</b><p>'+$canvas.attr('data-address')+'</p>'
             });
                
         infowindow.open(map, marker);
         
          google.maps.event.addListener(marker, 'click', (function(marker) {
            return function() {
              //infowindow.setContent("Test");
              infowindow.open(map, marker);
            }
          })(marker));  
            
        });
    }
    
    
	/*Ajax Contact form ================================*/
	
	// Contact Form
  function ST_Contact_Form(){
        var error_report;
        jQuery(".st_contact_form").submit(function(){
            var f = jQuery(this);
    
            // Hide notice message when submit
            jQuery(".notice_ok",f).hide();
            jQuery(".notice_error",f).hide();
            error_report = false;
            
            var is_valid = f.valid();
            
            
           if(is_valid){
               
                jQuery(".loading",f).removeClass('hide').show();
        
                var string_data = "action=contact_send&ajax=true&";
                string_data +=  f.serialize();
    
                jQuery.ajax({
                    type: "POST",
                    url: window.ajax_url,
                    data: string_data,
                    success: function(response){
                        jQuery(".loading",f).addClass('hide').hide();
                        if(response == 'success'){
                            jQuery(".notice_ok",f).removeClass('hide').show();
                            jQuery(".notice_error",f).addClass('hide').hide();
                        } else {
                            jQuery(".notice_error",f).removeClass('hide').show();
                            jQuery(".notice_ok",f).addClass('hide').hide();
                        }
                    }
                
                });
            
            }
            
    
            return false;
        });
    
    } // end function ST_Contact_Form
    
    
    
    function ST_CheckAvailability(){
         var error_report;
         jQuery(".check_avail").submit(function(){
             
             //var f = jQuery(this).parents('.check_avail');
            var f = jQuery(this);
    
            // Hide notice message when submit
            jQuery(".notice_ok",f).hide();
            jQuery(".notice_error",f).hide();
            error_report = false;
            
            var is_valid = f.valid();
            
            
           if(is_valid){
               
                jQuery(".loading",f).removeClass('hide').show();
        
                var string_data = "action=reservation_send&ajax=true&";
                string_data +=  f.serialize();
    
                jQuery.ajax({
                    type: "POST",
                    url: window.ajax_url,
                    data: string_data,
                    success: function(response){
                        jQuery(".loading",f).addClass('hide').hide();
                        if(response == 'success'){
                            jQuery(".notice_ok",f).removeClass('hide').show();
                            jQuery(".notice_error",f).addClass('hide').hide();
                        } else {
                            jQuery(".notice_error",f).removeClass('hide').show();
                            jQuery(".notice_ok",f).addClass('hide').hide();
                        }
                    }
                
                });
            
            }
            
    
            return false;
        });
        
    }// end ST_CheckAvailability
    
    
    function ST_event_calendar(){
    
    jQuery('.st-events-calendar .header a').live('click',function(){
         var p = jQuery(this).parents('.st-events-calendar');
         var  q = jQuery(this).attr('data');
         
        
        jQuery(".loading",p).show();
        var string_data = "action=ajax_events_calendar&ajax=true&"+q;
      
        jQuery.ajax({
            type: "GET",
            url: window.ajax_url,
            data: string_data,
            success: function(response){
                jQuery(".loading",p).hide();
                jQuery('.events-calendar-ajax',p).html(response);
            }
        });
        
        return false;
    });
      
}
    
	
	ST_Contact_Form();
	ST_CheckAvailability();
	ST_event_calendar();
	
	
	/* Full Screen slider =========================*/
	if(typeof(window.full_slider_data)!='undefined'  && typeof(jQuery.supersized)!='undefined'){
        jQuery('html').addClass('supersized-appened');
    	 jQuery.supersized({
                    
            //Functionality
            slideshow               :   window.full_slider_settings.slideshow,      //Slideshow on/off
            autoplay                :   window.full_slider_settings.autoplay,      //Slideshow starts playing automatically
            start_slide             :   1,      //Start slide (0 is random)
            random                  :   0,      //Randomize slide order (Ignores start slide)
            slide_interval          :   window.full_slider_settings.slide_interval,   //Length between transitions
            transition              :   1,      //0-None, 1-Fade, 2-Slide Top, 3-Slide Right, 4-Slide Bottom, 5-Slide Left, 6-Carousel Right, 7-Carousel Left
            transition_speed        :   window.full_slider_settings.transition_speed,    //Speed of transition
            new_window              :   1,      //Image links open in new window/tab
            pause_hover             :   0,      //Pause slideshow on hover
            keyboard_nav            :   1,      //Keyboard navigation on/off
            performance             :   1,      //0-Normal, 1-Hybrid speed/quality, 2-Optimizes image quality, 3-Optimizes transition speed // (Only works for Firefox/IE, not Webkit)
            image_protect           :   1,      //Disables image dragging and right click with Javascript
            image_path              :   'img/', //Default image path
    
            //Size & Position
            min_width               :   0,      //Min width allowed (in pixels)
            min_height              :   300,        //Min height allowed (in pixels)
            vertical_center         :   0,      //Vertically center background
            horizontal_center       :   1,      //Horizontally center background
            fit_always              :   0,
            fit_portrait            :   0,      //Portrait images will not exceed browser height
            fit_landscape           :   0,      //Landscape images will not exceed browser width
            
            //Components
            navigation              :   1,      //Slideshow controls on/off
            thumbnail_navigation    :   0,      //Thumbnail navigation
            slide_counter           :   1,      //Display slide numbers
            slide_captions          :   1,      //Slide caption (Pull from "title" in slides array)
            slides                  :   window.full_slider_data
            
        });
    
	   
	} // end check slider data


    /* For Twitter */
  
    jQuery('.twitter-update ul').each(function(){
        var  id = jQuery(this).attr('id');
        if(id!='' && typeof(window['twitter_'+id])!='undefined'){
            jQuery("#"+id).tweet(window['twitter_'+id]);
        }
    });
    
     // back to top
    jQuery(window).scroll(function() {
        if(jQuery(this).scrollTop() != 0) {
            jQuery('#sttotop').fadeIn();    
        } else {
            jQuery('#sttotop').fadeOut();
        }
    });
 
    jQuery('#sttotop').click(function() {
        jQuery('body,html').animate({scrollTop:0},800);
    }); 
     
        
      /* PRETTY PHOTO ======================================== */
     
    jQuery("a[data-rel^='prettyPhoto']").prettyPhoto({
        social_tools: '',
        overlay_gallery: false
    });
    
    /*
    jQuery("a[rel^='prettyPhoto']").prettyPhoto({
        social_tools: ''
    });
    */

    
	

	
 /* END jQuery document Ready */
});

