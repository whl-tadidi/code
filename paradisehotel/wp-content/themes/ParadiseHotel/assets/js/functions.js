

jQuery(document).ready(function(){  
    var ajaxurl = window.ajaxurl || window.ajax_url || '';
    // HOVER IMAGE MAGNIFY===============================================================================
     //Set opacity on each span to 0%
    jQuery(".magnify").css({'opacity':'0'});

    jQuery('.picture a').hover(
        function() {
            jQuery(this).find('.magnify').stop().fadeTo(800, 1);
        },
        function() {
            jQuery(this).find('.magnify').stop().fadeTo(800, 0);
        }
    )
    
    
    /* SUPERFISH MENU ====================================== */
jQuery('ul.sf-menu').superfish({ 
            delay:       800,                            // one second delay on mouseout 
            animation:   {opacity:'show',height:'show'},  // fade-in and slide-down animation 
            speed:       'normal',                          // faster animation speed 
            autoArrows:  false,                           // disable generation of arrow mark-up 
            dropShadows: false                            // disable drop shadows 
        }); 

    jQuery('.tooltip_1').poshytip({
                className: 'tip-twitter',
                showTimeout: 1,
                alignTo: 'target',
                alignX: 'center',
                offsetY: 5,
                allowTipHover: false,
                fade: true,
                slide: true
            })

    
/* WEATHER ======================================== */
    
    var wheather_settings = window.st_wheather || false;
   if(wheather_settings!= false  && wheather_settings.show=='y' ){
       
       jQuery.simpleWeather({
        zipcode: '',
        woeid: wheather_settings.woeid,
        location: '',
        unit: wheather_settings.unit,
        success: function(weather) {

            jQuery('#wxIcon2').append('<img src="'+weather.image+'" alt="" />');
            jQuery('#wxTemp').html(weather.temp + '&deg;' + (weather.units.temp.toUpperCase()));
            jQuery('#wxIntro').text(weather.city);

        },
        error: function(error) {
          //$("#weather").html('<p>'+error+'</p>');
        }
      });
       

   
    } // end if show
    
    /* MOBILE MENU ======================================== */
    jQuery('#main-nav').mobileMenu();
    
        
/* RATING ======================================== */
        
        jQuery('.rate_wd').each(function(i) {
            var widget = this;
            var id = jQuery(widget).attr('id');
            id = id.replace('r-','');
            var out_data = {
                widget_id : id,
                fetch: 1,
                action: 'room_vote'
            };
            jQuery.post(
                ajaxurl,
                out_data,
                function(INFO) {
                    jQuery(widget).data( 'fsr', INFO );
                    set_votes(widget);
                },
                'json'
            );
        });
    

        jQuery('.ratings_stars').hover(
            // Handles the mouseover
            function() {
                jQuery(this).prevAll().andSelf().addClass('ratings_over');
                jQuery(this).nextAll().removeClass('ratings_vote'); 
            },
            // Handles the mouseout
            function() {
                jQuery(this).prevAll().andSelf().removeClass('ratings_over');
                // can't use 'this' because it wont contain the updated data
                set_votes(jQuery(this).parent());
            }
        );
        
        
        // This actually records the vote
        jQuery('.ratings_stars').bind('click', function() {
            var star = this;
            var widget = jQuery(this).parent();
            var id = jQuery(star).parent().attr('id');
            
            if(getCookie('voted-'+id)==1){
                 var note = widget.attr('voted-text') || '';
                 if(note!=''){
                     alert(note);
                 }
                 return false;
            }else{
                setCookie('voted-'+id,'1',1);
            }
            
            
            
            id = id.replace('r-','');
            var clicked_data = {
                clicked_on : jQuery(star).attr('class'),
                widget_id : id,
                 action: 'room_vote'
            };
            jQuery.post(
                 ajax_url,
                clicked_data,
                function(INFO) {
                    widget.data( 'fsr', INFO );
                    set_votes(widget);
                    
                    
                },
                'json'
            ); 
        });
        

    
// EXPOSE FUNCTIONS
    // expose the form when it's clicked or cursor is focused
    var form = jQuery(".expose").bind("click keydown", function() {

        jQuery(this).expose({

            // when exposing is done, change form's background color
            onLoad: function() {
                form.css({backgroundColor: '#f6f6f6'});
            },

            // when "unexposed", return to original background color
            onClose: function() {
                form.css({backgroundColor: null});
            }

        });
    });
    
    
});


function set_votes(widget) {

        var avg = jQuery(widget).data('fsr').whole_avg;
        var votes = jQuery(widget).data('fsr').number_votes;
        var exact = jQuery(widget).data('fsr').dec_avg;
        var  vote_tex = jQuery(widget).data('fsr').vote_txt;
    
     //   window.console && console.log('and now in set_votes, it thinks the fsr is ' + jQuery(widget).data('fsr').number_votes);
        
        jQuery(widget).find('.star_' + avg).prevAll().andSelf().addClass('ratings_vote');
        jQuery(widget).find('.star_' + avg).nextAll().removeClass('ratings_vote'); 
        jQuery(widget).find('.total_votes').text( vote_tex );
    }







