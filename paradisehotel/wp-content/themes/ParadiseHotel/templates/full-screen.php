<?php 
global $post;
the_post();
$st_page_options =  $st_page_builder = get_page_builder_options($post->ID);
$builder_content = get_page_builder_content($post->ID) ;


if(isset($st_page_options['slider_items']) && isset($st_page_options['slider_items']['images'])  && is_array($st_page_options['slider_items']['images']) ){
    
$images = $st_page_options['slider_items']['images'];
$metas = $st_page_options['slider_items']['meta'];

$sliders= array();

 // echo var_dump($images); 
 
foreach($images as $i => $img_id){
     $attachment=wp_get_attachment_image_src($img_id, 'full');
     $img_url =  $attachment[0];
     $item = array();
     $item['image'] =  $img_url;
     $item['title'] ='';
     $meta  = $metas[$i];
     
     
     
     if($meta['title']!='' || $meta['caption']!='' ){
         if($meta['url']!='' && $meta['title']!='' ){
                 $item['title'] ='<a href="'.$meta['url'].'">'.esc_html($meta['title']).'</a>';
            }else if($meta['title']!='' ){
                 $item['title'] = esc_html($meta['title']);
            }
            
        if($meta['caption']!=''){
              
              $item['title'].= '<span>'.esc_html($meta['caption']).'</span>';
          }
             
     } 
            
          
    $sliders[] =  $item;


}
$slider_settings = array();

$slider_settings['slideshow'] = st_get_setting("fsc_slideshow")=='n' ? 0 :  1;
$slider_settings['autoplay'] = st_get_setting("fsc_autoplay")=='n' ? 0 : 1;
$slider_settings['slide_interval'] = intval(st_get_setting("fsc_interval")) > 0 ? intval(st_get_setting("fsc_interval")) :  6000;
$slider_settings['transition_speed'] = intval(st_get_setting("fsc_transition_speed")) > 0 ? intval(st_get_setting("fsc_transition_speed")) :  800;



?>

<div class="full-screen-wrap">
    <script type="text/javascript">
        var full_slider_settings=<?php echo json_encode($slider_settings) ?>;
        var full_slider_data = <?php echo json_encode($sliders); ?>;
        
    </script>    
     <!--Arrow Navigation-->
    <a id="prevslide" class="load-item"></a>
    <a id="nextslide" class="load-item"></a>
    <div id="slidecaption" ></div>
</div>
<?php 

} // end if is images
