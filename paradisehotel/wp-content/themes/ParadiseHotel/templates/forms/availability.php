<!-- start check avail-->
<div id="check_avail" class="expose add-bottom  ">
    <?php  if(isset($settings['title'])){ ?>
	<h2><?php echo esc_html($settings['title']); ?></h2>
	<?php } ?>
	
	 <?php echo  balanceTags($settings['before_form']); ?>
	<form action="#" method="post" id="myform" class="validate-form check_avail">
		<fieldset class="col_f_1">
			<label><?php _e('Check in','smooththemes'); ?></label>             <input name="checkin" type="text" class="required calendar date-input">
		</fieldset>
		<fieldset class="col_f_2">
			<label><?php _e('Check out','smooththemes'); ?></label> <input name="checkout" type="text" class="required calendar date-input">
            
		</fieldset>
		<br class="clear">
		<fieldset class="col_f_1">
			<label><?php _e('N°of guest','smooththemes'); ?></label>
			<select name="reservation_guest" class="required">
				<option value=""><?php _e('- please select -','smooththemes'); ?></option>
                <?php for($i=1;  $i<=8;  $i++){ ?>
				<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                <?php } ?>
				
			</select>
           
		</fieldset>
		<fieldset class="col_f_2">
			<label><?php _e('Room type','smooththemes') ?></label>
			<select name="reservation_type" class="required">
				    <option value=""><?php _e('- please select -','smooththemes'); ?></option>
                        <?php 
                        // added in ver 1.3
                        $args = array();
                        $args['posts_per_page'] ='-1';
                        $args['orderby'] = 'post_title';
                        $args['order'] = 'ASC';
                        $args['post_type']='room';
                        if(st_is_wpml()){
                              $args['sippress_filters'] = true;
                              $args['language'] = get_bloginfo('language');
                         }
                          
                         //  echo var_dump($wp_query);
                         $new_query = new WP_Query($args);
                         $myposts = $new_query->posts;
                          foreach($myposts as $p){
                               ?>
                                <option value="<?php echo esc_attr($p->post_title); ?>"><?php echo esc_html( $p->post_title ); ?></option>
                               <?php
                          }
                         
                        wp_reset_query(); ?>
                   
			</select>
            
		</fieldset>
		<br class="clear "/>
		<fieldset>
			<label><?php _e('Your Name','smooththemes'); ?></label><input type="text" name="reservation_name" class="required long"/>
   
			<label><?php _e('Your Email','smooththemes'); ?></label><input type="email" name="reservation_email" class="required email long">
            
		</fieldset>
		
		  
        <p class="notice notice_ok hide"><?php _e('Thank you for contacting us. We will get back to you as soon as possible.','smooththemes'); ?></p>
        <p class="notice notice_error hide"><?php _e('Due to an unknown error, your form was not submitted. Please resubmit it or try later.','smooththemes'); ?></p>
    
		
    		
    	<p class="b0">
        	<button type="submit"><?php _e('Submit','smooththemes'); ?></button>
    		<span class="loading hide"></span>
        </p>
		
		<input type="hidden" value="<?php echo isset($settings['mail_to']) ?  $settings['mail_to'] : ''; ?>" name="to_email" />
	</form>
	
	 <?php echo  balanceTags($settings['after_form']); ?>
	
</div><!-- end check avail-->