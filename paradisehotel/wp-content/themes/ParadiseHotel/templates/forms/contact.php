<form method="post" class="st_contact_form  validate-form" action="">
 	<div class="row">
		<div class="six column ">
			<fieldset>
				<label><?php _e('Name','smooththemes'); ?></label>
				<input name="contact_name" class="required long" type="text"/>
				<label><?php _e('Last Name','smooththemes') ?></label>
				<input name="contact_last_name" class="required long" type="text"/>
				<label><?php  _e('Email','smooththemes');  ?></label>
				<input name="contact_email" class="required email long" type="email"/>
			</fieldset>
		</div>
		<div class="six column omega">
			<fieldset>
				<label><?php _e('Message','smooththemes') ?></label>
				<textarea  name="contact_message" class="required" style="width:95%; height:75px"></textarea>
			</fieldset>
			
			<p class="notice_ok hide"><?php _e('Thank you for contacting us. We will get back to you as soon as possible.','smooththemes'); ?></p>
            <p class="notice notice_error hide"><?php _e('Due to an unknown error, your form was not submitted. Please resubmit it or try later.','smooththemes'); ?></p>
	   		
			<button  type="submit" class="button" ><?php _e('Send Message','smooththemes');  ?></button>
			
			<span class="loading hide"></span>
			<input type="reset" class="hide" value="" />
			

		</div>
	</div>
	
	<input type="hidden" name="to_email" value="<?php echo esc_attr($data['to_email']); ?>" />
</form>

