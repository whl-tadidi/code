<?php
global $post, $page;
the_post(); 
$st_page_options =  $st_page_builder = get_page_builder_options($post->ID);
$builder_content = get_page_builder_content($post->ID); 
 
?>
<div class="page-content">
        <?php 
          $thumb_html = st_post_thumbnail($post->ID,'st_large',false, true);
         if($page<2 && $thumb_html!=''): 
         ?>
        <div class="page-featured-image cpt-thumb-wrapper">
        <?php echo $thumb_html;?>
        </div>
      <?php endif;  
      
       // service included
     
        do_action('st_before_the_content',$post);
         echo '<div class="text-content">';
            the_content(); 
         echo '</div>';
        do_action('st_after_the_content',$post);
        do_action('st_after_page_content');
        
        
          ?>
        <div class="services-included">
             <?php echo st_room_services($post->ID,false,'all'); ?>
            <div class="clear"></div>
        </div>  
        
          <div class="room_rating" >
              <?php  echo st_room_rate($post->ID); ?>
          </div>                         
        <?php 
        
        
        $args = array(
          'before'           => '<p class="single-pagination">' . __('Pages:','smooththemes'),
          'after'            => '</p>',
          'link_before'      => '',
          'link_after'       => '',
          'next_or_number'   => 'number',
          'nextpagelink'     => __('Next page','smooththemes'),
          'previouspagelink' => __('Previous page','smooththemes'),
          'pagelink'         => '%',
          'echo'             => 1
        );
        wp_link_pages( $args ); 
        
         ?> 
        <div class="clear"></div>
 </div><!-- END page-content-->
    
    <div class="page-single-element">
    <?php /*
        <p class="page-tags">
            <?php echo get_the_term_list($post->ID,'room_cat','<b>'.__('Category:','smooththemes').'</b> ',', ',''); ?>
        </p>
        <div class="clear"></div>
    */ ?>

        <?php  do_action('st_before_author'); ?>
        
        <div class="clear"></div>
    </div><!-- Single Page Element -->