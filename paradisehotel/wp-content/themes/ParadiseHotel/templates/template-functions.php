<?php 
function st_get_tpl_file_name(){

  $default = 'list-post';
  $file ='list-post';
  
    if(is_singular()){
        global $post;
        if($post->post_type!='page' && $post->post_type!='post'){
             $file = $post->post_type;
             if(!file_exists(ST_TEMPLATE_DIR.$file.'.php')){ 
                $file ='single';
             }
             
        }else{
            $file = 'single';
             if(is_page()){
                 $file = 'page';
             }
        }
       }elseif(is_author()){ 
           $file = 'author';
       }elseif(is_tag()){
            $file = 'tag';
       }elseif(is_tax()){
             $tax = get_queried_object();
             $file = 'taxonomy-'.$tax->taxonomy;
             if(file_exists(ST_TEMPLATE_DIR.$file.'.php')){  
                   return $file;  
            }else{
                return 'taxonomy';
            }
              
       } elseif( (is_archive() || is_day() || is_date() || is_month() || is_year() || is_time()) && !is_category() ){
             $file = 'archive';
       }elseif(is_search() ){
             $file = 'search';
       }elseif(is_404() ){
             $file = '404';
       }
      
       if(file_exists(ST_TEMPLATE_DIR.$file.'.php')){  
            return $file;  
        }else{
            return $default;
        } 
        
}


function st_page_title_tempalte(){
     $file = ST_TEMPLATE_DIR.'top-title.php'; // defaults
       if(file_exists($file)){
            include($file);
       }
}

/**
 * hook : st_page_title_tempalte
*/
add_action('st_top_page_template','st_page_title_tempalte',10  ); 


/**
 * Include current template for  layout
*/
function st_page_template(){
       $default = 'list-post';
       // for title
       $file = $GLOBALS['st_template_file_name'];
       // for main content 
       if(file_exists(ST_TEMPLATE_DIR.$file.'.php')){
            include(ST_TEMPLATE_DIR.$file.'.php');
       }else{
            include(ST_TEMPLATE_DIR.$default.'.php');
       }   
}
/**
 * hook : st_page_template
*/
add_action('st_page_template','st_page_template');


/**
 * display sidebar depend each page
 */ 
function st_sidebar($sidebar ='',$positon ='right'){
    $sidebar ;
    
    $afterfix ='_r';
     if(strtolower($positon)=='left'){
        $afterfix='_l';
     }
    
    if(empty($sidebar)){
        if(is_category()){
            $sidebar = st_get_setting("sidebar_category".$afterfix,'sidebar_default'.$afterfix);
        }elseif(is_search()){
            $sidebar = st_get_setting("sidebar_search".$afterfix,'sidebar_default'.$afterfix);
        }
    }
    
     if(empty($sidebar) || $sidebar==''){
        $sidebar = 'sidebar_default'.$afterfix;
     }
     

    do_action('st_before_sidebar'.$afterfix,$sidebar);
    dynamic_sidebar($sidebar); 
    do_action('st_atter_sidebar'.$afterfix,$sidebar);
}
/**
 * hook st_sidebar
 */ 
add_action('st_sidebar','st_sidebar',10,2);



/**
 * Return laoyout file by number
*/
function st_get_layout($number =-1){
    if($number<1){
    
       if(is_singular()){
            global $post;
         
            $st_page_builder = get_page_builder_options($post->ID);
            $layout = (isset($st_page_builder['layout'])) ? intval($st_page_builder['layout']) : 0;
            
            if(isset($st_page_builder['page_template'])  && $st_page_builder['page_template'] =='home'){
                $layout = 4;
            }elseif(isset($st_page_builder['page_template'])  && $st_page_builder['page_template'] =='full'){
                $layout = 5;
            }

            
            if(in_array($layout,array(1,2,3,4,5))){
               $number = $layout  ;
            }
            
        }elseif(is_tax()){ // for default layout in admin page 
             $tax = get_queried_object();
             $number  =  intval(st_get_setting("{$tax->taxonomy}_layout",0));        
        }
        
        if($number<=0){
            $number  =  intval(st_get_setting("layout",2));  
        }
        
    } // end if number 
    
    
    switch(intval($number)){
          case  5 : 
            $l = 'layout-fullscreen';
        break;
        case  4 : 
            $l = 'layout-home';
        break;
          case  3 : 
            $l =  'layout-left-sidebar';
        break;
        case  2 : 
            $l =  'layout-right-sidebar';
        break;
        case  1 : 
            $l =  'layout-no-sidebar';
        break;
        default :
          $l = 'layout-right-sidebar';
  }
  
  return  apply_filters('st_get_layout',$l,$number);
}


/**
 * Add page type to body class
 * */
 
 function st_body_class($classes= false, $class =''){
     
     if(is_singular()){
            global $post;
         
            $st_page_builder = get_page_builder_options($post->ID);
            $layout = (isset($st_page_builder['layout'])) ? intval($st_page_builder['layout']) : 0;
            
            if(isset($st_page_builder['page_template'])  && $st_page_builder['page_template'] =='home'){
                $classes[] = 'st-home';
            }elseif(isset($st_page_builder['page_template'])  && $st_page_builder['page_template'] =='full'){
                 $classes[]='st-full-screen';
            }else{
                $classes[]='st-default';
            }

            
            if(in_array($layout,array(1,2,3,4,5))){
               $number = $layout  ;
            }
            
        }
     
     return  $classes;
 }
 
 /**
  * Aplly to hook _body_class
  * 
  * @function st_body_class();
  * */
  
add_filter('body_class', 'st_body_class');
 




// this is call back for comments
function st_comments($comment, $args, $depth) {
   $GLOBALS['comment'] = $comment; ?>
   <li <?php comment_class('comment'); ?> id="li-comment-<?php comment_ID() ?>">
     <div id="comment-<?php comment_ID(); ?>" class="comment-item">
     
      <div class="comment-header">
  	 <?php echo get_avatar($comment->comment_author_email,$size='60',$default='' ); ?>
    
      <div class="comment-header-right">
            <p class="comment-date"><?php printf(__st('%1$s'), get_comment_date()); ?></p>
            <a href="#" class="comment-author"><?php printf('<b class="author_name">%s</b>', get_comment_author_link()) ?></a>
            <?php edit_comment_link(__st('(Edit)'),'  ','') ?>
            <?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
        </div>
      
        <div class="clear"></div>
      </div>
      
      <div class="clear"></div>
      
      <div class='comment-content'>
          <?php comment_text() ?>
          <?php if ($comment->comment_approved == '0') : ?>
            <br /> <em><?php _st('Your comment is awaiting moderation.','smooththemes') ?></em>
          <?php endif; ?>

      </div>
     </div>
<?php
}


/**
 * parse Font
 * @return array
*/
function st_parse_font($font_url){
    $font_url  = urldecode($font_url);
    $args =  parse_url($font_url);
    $return = array('is_g_font'=> false, 'name'=>$font_url,'link'=>'');
    
    $args = wp_parse_args($args, array(
        'host'=>'',
        'query'=>''
    ));
    
    $font_data = wp_parse_args($args['query'], array('family'=>'','subset'=>''));
    
    if($args['host']=='fonts.googleapis.com' && $font_data['family']!='' ){
      //  echo var_dump($args) ; die();
        
        if(strpos($font_data['family'],':')!==false){
            $font_data['family'] = explode(':',$font_data['family']);
            $font_data['family'] =  (isset($font_data['family'][0])  && $font_data['family'][0]!='') ? $font_data['family'][0]  : '';
        }else{
            
        }
        
        if($font_data['family']!=''){
            $return['name'] = $font_data['family'];
              $return['is_g_font'] = true;
              $return['link'] = $font_url;   
        }
    }
        
  return $return;  
}


/**
 * make font style
 * Only use for header.php file
*/
function st_make_font_style($font,$css_selector,$show_font_size= true){
    
  
    
if($font['font-family']!=''){
    $font_data = st_parse_font($font['font-family']);
    
//$is_not_gfont = key_exists($font['font-family'],st_get_normal_fonts()); 
?>
<?php if($font_data['is_g_font']==true) : ?>
<link href='<?php echo  $font_data['link'] ?>' rel='stylesheet' type='text/css'>
<?php endif; ?>

<style type="text/css">

<?php echo $css_selector; ?>{ 
    font-family: '<?php echo $font_data['name']; ?>'; 
    <?php if(isset($font['font-style']) && $font['font-style']): ?>
    font-style: <?php echo $font['font-style']; ?>;
    <?php endif; ?>
    <?php if(isset($font['font-style']) && $font['font-style']): ?>
    font-style: <?php echo $font['font-style']; ?>;
    <?php endif; ?>
    <?php if(isset($font['font-weight']) && $font['font-weight']): ?>
    font-weight: <?php echo $font['font-weight']; ?>;
    <?php endif; ?>
    <?php if(isset($font['font-size']) && $font['font-size']): ?>
    font-size: <?php echo intval($font['font-size']); ?>px;
    <?php endif; ?>
    <?php if(isset($font['line-height']) && $font['line-height']): ?>
    line-height: <?php echo intval($font['line-height']); ?>px;
    <?php endif; ?>
    <?php if(isset($font['color'])  && $font['color']): ?>
    color: #<?php echo $font['color']; ?>;
    <?php endif; ?>
    
}
</style>
<?php
     }
}




/** ************* ST Theme ads ********************/
/**
 *  auto add ads to hooks
*/
function st_auto_ads(){
    $ads = st_get_setting("ads");
    if(is_array($ads)){
        foreach($ads as $ad){
            if($ad['hook']!=''&& $ad['content']!=''){
                $ad['content'] = stripslashes($ad['content']);
                $ad['content']=  str_replace("'","\'",  $ad['content']);
                $new_func = create_function('$c=""',' echo  \''.$ad['content'].'\' ; ');
                add_action($ad['hook'],$new_func);
            }
        }
    }
    
}
st_auto_ads(); // auto run


function st_background_sytle($bg_color='',$bg_img='',$bg_positon='',$bg_repreat='',$bg_fixed='n'){
    $bd_style ='';
     if($bg_color!='' ||  $bg_positon!='' || $bg_img!=''){
        
      if($bg_color!=''){
          $bd_style .= ' #'.$bg_color; 
      }   
       if($bg_img!=''){
          $bd_style .= ' url('.$bg_img.') '; 
         
              switch(strtolower($bg_positon)){
                    case 'tl':
                        $bd_style.=' top left ';
                    break;
                    
                    case 'tr':
                        $bd_style.=' top right ';
                    break;
                    
                    case 'tc':
                        $bd_style.=' top center ';
                    break;
                    
                    case 'cc':
                        $bd_style.=' center center';
                    break;
                    case 'bl':
                        $bd_style.=' bottom left ';
                    break;
                     case 'br':
                        $bd_style.=' bottom right ';
                    break;
                     case 'bc':
                        $bd_style.=' bottom center ';
                    break;
              }
              
           switch(strtolower($bg_repreat)){
                    case 'x':
                        $bd_style.=' repeat-x ';
                    break;
                    case 'y':
                        $bd_style.=' repeat-y ';
                    break;
                    case 'n':
                        $bd_style.=' no-repeat ';
                    break;
           }
           
           if($bg_fixed=='y'){
                $bd_style.=' fixed ';
           }
      } 
  }
  
  return $bd_style ;
}


/**
 * Set back ground for header
 * hook wp_head
*/
function st_theme_header_bg(){
    
    if(st_get_setting('disable_header_custom','n')=='y'){
        return ;
    }
    
    $bd_style= '';
    $bg_color = $bg_img = $bg_positon =$bg_repreat = $bg_fixed ='';

    $bg_color = st_get_setting("header_bg_color",'');
    $bg_img     =  st_get_setting("header_bg_img",'');
    $bg_positon = st_get_setting("header_bg_positon",'');
    $bg_repreat = st_get_setting("header_bg_repreat",'');
    $bg_fixed   = st_get_setting('header_bg_fixed','n');
    
    $bd_style =  st_background_sytle($bg_color,$bg_img, $bg_positon, $bg_repreat,$bg_fixed);
    
    $link_color =  st_get_setting("header_link_color",'202020');
    $link_hover_color =  st_get_setting("header_link_hover_color",'80B500');
      if($bd_style!='' || $link_hover_color!='' ||  $link_color!=''){
         echo '<style type="text/css">' ; 
             if($bd_style!=''){
                 echo '#header .header-outer-wrapper{background: '.$bd_style.'; }';
             }
             
             if($link_color!=''){
                  echo '#header .header-outer-wrapper .primary-nav > ul > li > a{color: #'.$link_color.'; }';
             }
             
             if($link_hover_color!=''){
                  echo '#header .header-outer-wrapper .primary-nav  >ul > li > a:hover,
                  #header .header-outer-wrapper .primary-nav  >ul > li.current-menu-item > a,
                  #header .header-outer-wrapper .primary-nav  >ul > li.current-menu-parent > a {color: #'.$link_hover_color.'; }';
             }
             
             
        echo  '</style>';
      }
}



/**
 * Set back ground for body
 * hook  wp_head
*/
function st_theme_body_bg(){
// For background settings
/*
    $bg_type  = st_get_setting("bg_type",'d');
    if($bg_type=='d'){
          $bg = st_get_setting("defined_bg",'background1.jpg');
          // large image with fixed
          if(in_array($bg,array('background1.jpg'))){
              $bg = ST_THEME_URL.'assets/images/patterns/'.$bg;
               $style ='background: url("'.$bg.'") no-repeat fixed center center / cover  transparent;';
          }else{
              $bg = ST_THEME_URL.'assets/images/patterns/'.$bg;
              $style ='background: url("'.$bg.'") repeat  center center ';
          }
          
          echo '<style type="text/css">body {'.$style.' }</style>';
         return ;
    }elseif($bg_type=='c'){
         $bg = st_get_setting("defined_bg_color");
         if($bg!=''){
             echo '<style type="text/css">body {background: #'.$bg.'; }</style>';
         }
         return ;
    }
    
    // if is custom background

     $bd_style= '';
     $bg_color = $bg_img = $bg_positon =$bg_repreat = $bg_fixed ='';

     $bg_color = st_get_setting("bg_color",'');
 
    $bg_img     =  st_get_setting("bg_img",'');
    $bg_positon = st_get_setting("bg_positon",'');
    $bg_repreat = st_get_setting("bg_repreat",'');
    $bg_fixed   = st_get_setting('bg_fixed','n');
    
    $bd_style =  st_background_sytle($bg_color,$bg_img, $bg_positon, $bg_repreat,$bg_fixed);
    
      if($bd_style!=''){
         echo '<style type="text/css">body {background: '.$bd_style.'; }</style>';
      }
*/ 
}


function st_theme_font(){
    
      $font_body = st_get_setting("body_font",array('font-family'=>'Roboto'));
      $heading_font = st_get_setting("headings_font",array('font-family'=>'Roboto'));
      st_make_font_style($font_body,'body');
      st_make_font_style($heading_font,'h1,h2,h3,h4,h5,h6,.text-content h1, .subscribe_section label, .widget_calendar  caption, .title, .st-tabs .tab-title li.current');
      // Predefined Colors (pc) - Custom Color (cc)
      ?>
      <style type="text/css">
          h1.page-title{ color: #<?php echo esc_attr(st_get_setting("page_title_color",'444444')); ?> ;}
          h1.page-title span{ color: #<?php echo esc_attr(st_get_setting("page_tagline",'999999')); ?> ;}
          body a{ color: #<?php echo esc_attr(st_get_setting("link_color",'006699')); ?> ; }
          body a:hover{ color: #<?php echo esc_attr(st_get_setting("ink_color_hover",'006699')); ?> ; }
          
         body  .button, body  a.button, body  .btn{ color: #<?php echo esc_attr(st_get_setting("btn_color",'444444')); ?> ; }
         body .button:hover,  body a.button:hover, body .btn:hover{ color: #<?php echo esc_attr(st_get_setting("btn_color_hover",'444444')); ?> ; }
      </style>
      <?php
}

// add to wp_head
add_action('wp_head','st_theme_font',90);
add_action('wp_head','st_theme_body_bg',91);
add_action('wp_head','st_theme_header_bg',92);

function st_header_tracking_code(){
    $code = st_get_setting('headder_tracking_code','');
    $code = stripslashes($code);
    if(is_string($code)){
         echo $code;
    }
}

function st_footer_tracking_code(){
    $code = st_get_setting('footer_tracking_code','');
    $code = stripslashes($code);
    if(is_string($code)){
         echo $code;
    }
}

add_action('wp_head','st_header_tracking_code',123);
add_action('wp_footer','st_footer_tracking_code',123);


/* Back to top buttn*/
 function st_back_totop(){
     echo '<div id="sttotop" class="bg_color"><i class="icon-angle-up"></i></div>';
 }
 add_action('wp_footer','st_back_totop');
 
 
/*****************************************/
/**
 * Display top slider
*/ 
function st_top_slider($post_id, $class=""){
?>

    
        <div class="twelve column <?php echo esc_attr($class); ?>">
        <?php 
           global $post;
           $st_page_builder = get_page_builder_options($post_id);
           st_the_slider($st_page_builder);
         ?>
        </div><!-- end twelve columns -->
        
   

<?php
}
 
 
 
 /** 
  * get Room services
 */
function st_room_services($room_id,  $display = false, $number_show = 'all'){
      
       $cache_key = 'st_room_services_'.$room_id.'_'.$number_show;
       
       $html = wp_cache_get($cache_key);
       if($html==false){
                $html = array();
               $services_included =  get_post_meta($room_id,'_st_services_included',true);
               $room_services = false;
               if($services_included){
                
                $new_query = new WP_Query(array(
                    	'orderby'         => 'post__in',
                    	'order'           => '',
                        'post__in'=>$services_included,
                    	'post_type'       => 'room_service',
                    	'post_status'     => 'publish',
                        'posts_per_page'=>'-1'
                    ));
                    
                    $room_services =  $new_query->posts;
                      if($room_services){ 
                         foreach($room_services as $s){
                               $thumb_url='';
                                if(has_post_thumbnail($s->ID)){
                                    $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($s->ID), 'thumbnail_size' );
                                    $thumb_url = $thumb['0'];
                                }
                                
                               $img =  ($thumb_url!='') ? '<img src="'.$thumb_url.'" alt="icon" />  ' : '';
                	           $title = get_the_title($s->ID);
                               $html[]=' <li> <a class="tooltip_1" href="#" title="'.esc_attr($title).'">'.$img.'</a> </li>';  
                                
                         }
                        
                      }
               }
               
               if(!is_numeric($number_show) || $number_show =='all'){
                  
               }else{
                    $number_show = intval($number_show);
                    if($number_show>= count($html)){
                        
                    }else{
                        $html = array_slice($html,0,$number_show);
                    }
                }
               
               $html = join('', $html);
               
              if($html!=''){
              
                $html = ' <ul class="room_facilities room-services"> '.$html. '</ul>';
                
                wp_cache_set($cache_key, $html);
              } 

        }
      
        if($display){
             echo  $html;
             return  false;
        }
        
        return $html;     
}

/**
 * Create rating template
 * 
 * @param  int $room_id
 * @return string
 * */
function st_room_rate($room_id){
    $html = '<div class="rate">
        	<div class="rate_wd" id="r-'.$room_id.'" voted-text="'.__('You voted for this room','smooththemes').'">
        		<div class="star_1 ratings_stars ratings_vote"></div>
                <div class="star_2 ratings_stars ratings_vote"></div>
        		<div class="star_3 ratings_stars ratings_vote"></div>
        		<div class="star_4 ratings_stars number_votes"></div>
        		<div class="star_5 ratings_stars number_votes"></div>
        		<div class="total_votes">'.__('Loading...','smooththemes').'</div>
        	</div>
        </div><!-- end rate-->';
    
    return $html;
}


/**
 * Return Gmap code
 * 
 * @param  array $mapData
 * @return string
 * */
function st_gmap_template($mapData = array()){
    
    if(
        !isset($mapData['latitude']) || !isset($mapData['longitude'] )
        || $mapData['latitude'] =='' || $mapData['longitude']==''
        
     ){
        return '';
    }
    
    
    $id = 'map-'.uniqid();
    
    $html = '
    <div class="map_canvas" map-id="'.$id.'" data-lat="'.esc_attr($mapData['latitude']).'" data-lng="'.esc_attr($mapData['longitude']).'" data-address="'.esc_attr($mapData['address']).'" data-zoom="'.esc_attr($mapData['zoom']).'" 
    data-maptitle="'.esc_attr($mapData['title']).'">
        <div class="gmap_canvas" id="'.$id.'"  style="height:'.esc_attr($mapData['height']).'px; width:100%;"></div>
    </div>
    ';
    return $html;
    
}












