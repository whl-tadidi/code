<?php
global $post;

if($post->ID):

$thumb_class ='post-thumbnail  hover-thumb'; // six
$content_class ='post-content'; // six
$is_small = false;
$pos_class ='blog-post-item b30';
$link =get_permalink($post->ID);
$date_format = get_option('date_format');

 // add event excerpt length
 add_filter( 'excerpt_length', 'st_post_excerpt_length', 999 );

?>

<div <?php post_class($pos_class); ?>  id="post-<?php the_ID(); ?>">

         <div class="news-title">
          <h3><a title="<?php printf( esc_attr__( 'Permalink to %s', 'smooththemes' ), the_title_attribute( 'echo=0' ) ); ?>" rel="bookmark"  href="<?php echo $link; ?>"><?php the_title(); ?></a></h3>
               
         </div>


        <div class="<?php echo $content_class; ?>">
  
            <?php if(!$is_small): ?>
            <div class="post-excerpt">
                <?php 

                  the_excerpt();
                ?>
            </div>
             <a href="<?php echo $link; ?>" class="button-2"><?php _e('Read more &rarr; ','smooththemes'); ?></a>
            <?php endif; ?>
        </div>
    <div class="clear"></div>   
</div>
<?php endif; 


// remove custom except lengthe for events
remove_filter('excerpt_length', 'st_post_excerpt_length');
        




