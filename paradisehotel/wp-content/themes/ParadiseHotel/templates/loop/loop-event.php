<?php
global $post;
if($post->ID):

 $st_page_options =  $st_page_builder = get_page_builder_options($post->ID);
 $builder_content = get_page_builder_content($post->ID); 
 
   $start_date = get_post_meta($post->ID,'_st_event_start_date',true);
    
    if($start_date!=''){
        $start_date = strtotime($start_date);
    }
    
    $end_date = get_post_meta($post->ID,'_st_event_end_date',true);
    if($end_date!=''){
        $end_date = strtotime($end_date);
    }
 

 $link = get_permalink($post->ID);

 //$address = get_post_meta($post->ID,'_st_event_meta_address',true);
 $price = get_post_meta($post->ID,'_st_event_meta_price',true);
 $map =  get_post_meta($post->ID,'_st_event_meta_map',true);
 if(!array($map)){
     $map =  array('address'=>'');
 }
 $address =  $map['address'];
 
 
 $image_size = 'st_medium';
 if(isset($data) && isset($data['image_size']) ){
      $image_size = $data['image_size'] ;
 }
 


?>
<div <?php post_class('blog-post-item event-post-item b30'); ?>  id="event-<?php the_ID(); ?>">
        
        <?php if($start_date): ?>
         <div class="event">
           <div class="event-data">
                 <strong> <a href="<?php echo $link; ?>"><?php echo date_i18n('d',$start_date); ?></a></strong><span><?php echo date_i18n('M',$start_date); ?></span>
          </div>
          <?php  endif; ?>
          <h3><a href="<?php echo $link; ?>"><?php  the_title(); ?></a></h3>
                <ul>
                    <?php if($address){ ?>
                    <li class="map-marker"><?php echo $address; ?></span></li>
                    <?php } // end addres ?>
                    <li class="time"><?php _e(sprintf(__('Start at %s','smooththemes'),date_i18n('H:iA',$start_date))) ?></li>
                    <li class="price"><?php echo $price; ?></li>
                </ul>
              <br class="clear">
          </div>
        
       
       
        <div class="clear"></div>
        
        <?php
          $thumb =  st_post_thumbnail($post->ID,$image_size);
          
          if($thumb){
         ?>
        <div class="blog-thumb-wrapper cpt-thumb-wrapper">
            <?php echo $thumb;  ?>
        </div>
        <?php } ?>
        
        <div class="blog-excerpt">
            <?php  the_excerpt(); ?>
        </div>
        <a href="<?php echo $link; ?>" class="button-2"><?php _e('Read more &rarr;','smooththemes'); ?></a>
    </div>

<?php endif; ?>