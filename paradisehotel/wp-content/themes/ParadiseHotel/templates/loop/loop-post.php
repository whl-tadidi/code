<?php
global $post;

if($post->ID):

$thumb_class ='post-thumbnail  hover-thumb'; // six
$content_class ='post-content'; // six
$is_small = false;
$pos_class ='blog-post-item b30';
$link =get_permalink($post->ID);
$date_format = get_option('date_format');

 // add event excerpt length
 add_filter( 'excerpt_length', 'st_post_excerpt_length', 999 );

?>

<div <?php post_class($pos_class); ?>  id="post-<?php the_ID(); ?>">
        <?php 
         
    
         global $authordata;
         
         $author_link = sprintf(  __('Posted by <a href="%1$s" title="%2$s" rel="author">%3$s</a>'),
                            esc_url( get_author_posts_url( $authordata->ID, $authordata->user_nicename ) ),
                            esc_attr( sprintf( __( 'Posted by %s','smooththemes' ), get_the_author() ) ),
                             get_the_author() 
                        );
         
        ?>
        
        
        <div class="news-title">
          <h3><a title="<?php printf( esc_attr__( 'Permalink to %s', 'smooththemes' ), the_title_attribute( 'echo=0' ) ); ?>" rel="bookmark"  href="<?php echo $link; ?>"><?php the_title(); ?></a></h3>
                <ul>
                    <li class="date"><?php printf('Posted on %s',get_the_time($date_format)); ?></li>
                    <li class="auth"><?php echo $author_link;  ?></li>
                    <li class="comments"><?php comments_number(__('0 Comment','smooththemes'),__('1 Comment','smooththemes'),__('% Comments','smooththemes') ); ?> </li>
                </ul>
              <br class="clear">
         </div>

        <?php
         $image_size = (isset($settings['image_size']) && $settings['image_size'] !='' ) ? $settings['image_size']  : 'st_medium';
         $thumb_html = st_post_thumbnail($post->ID,$image_size);
         if($thumb_html){
         ?>
        <div class="<?php echo $thumb_class; ?>  blog-thumb-wrapper">                     
            <?php
           
                echo $thumb_html;
            ?>
            
            <div class="clear"></div>
        </div>
        <?php } ?>
        
        <div class="<?php echo $content_class; ?>">
  
            <?php if(!$is_small): ?>
            <div class="post-excerpt">
                <?php 

                  the_excerpt();
                ?>
            </div>
             <a href="<?php echo $link; ?>" class="button-2"><?php _e('Read more &rarr; ','smooththemes'); ?></a>
            <?php endif; ?>
        </div>
    <div class="clear"></div>   
</div>
<?php endif; 


// remove custom except lengthe for events
remove_filter('excerpt_length', 'st_post_excerpt_length');
        




