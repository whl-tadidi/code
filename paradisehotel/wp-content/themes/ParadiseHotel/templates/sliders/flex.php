<?php


 if(empty($data['slider_items']['images'])){
        return '';
    }
    
    $images = $data['slider_items']['images'];
    $metas = $data['slider_items']['meta'];
    $id =  'slider-'.uniqid();
    if(isset($data['class'])  && $data['class']!=''){
        $data['class'] = ' '.trim($data['class']);
    }else{
        $data['class'] ='';
    }
    
    if(isset($data['slider_full_with'])  && $data['slider_full_with']==1){
        
    }
    
    
 
    ?>
 
    <div id="<?php echo $id; ?>" class="<?php echo !($is_top_slider) ? 'inside-post-slider ' : ''; ?> flexslider <?php echo $data['class']; ?>">
        <ul class="slides">
            <?php
            foreach($images as $n=> $img_id){
                $col = array();
               // $col['img'] = $img_id;
                $meta  = $metas[$n];
                $attachment=wp_get_attachment_image_src($img_id, $img_size);
                
                $item = '<li> %1$s </li>';
                $img = sprintf('<img src="%1$s" alt="" />',$attachment[0]);
                if($meta['url']!=''){
                        $img ='<a href="'.$meta['url'].'">'.$img.'</a></h3>';
                }
                
              $html_cap =  $caption = $title ='';
              
              if(isset($settings['show_caption'])  && strtolower($settings['show_caption'])=='no'){
                
              }else{
                 
                 if($meta['title']!='' || $meta['caption']!='' ){
                    if($meta['url']!='' && $meta['title']!='' ){
                        $title ='<h3><a href="'.$meta['url'].'">'.esc_html($meta['title']).'</a></h3>';
                    }else if($meta['title']!='' ){
                         $title = '<h3>'.esc_html($meta['title']).'</h3>';
                    }
                    
                 } 
                    
                    
              if($meta['caption']!=''){
                 $caption = '<div clas="ct">'.esc_html($meta['caption']).'</div>';
              } 

                
                if($meta['title']!='' || $meta['caption']!='' ){
                    $html_cap = '
                        <div class="flex-caption '.(($meta['caption']!='') ? 'hc' :'jt').'">
                            <div class="flex-caption-text">
                                    '.$title.'
                                    '.$caption.'
                                </div>
                        </div>';

                }
                 
              }
              
               $item = sprintf($item, $img.$html_cap); 
               echo  apply_filters('st_slider_item',$item,$img_id,$meta);    
            }
            ?>
        </ul>
    </div><!-- inside-post-slider -->
<?php 