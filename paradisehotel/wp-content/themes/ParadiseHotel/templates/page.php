<?php 
global $post;
the_post();
$st_page_options =  $st_page_builder = get_page_builder_options($post->ID);
$builder_content = get_page_builder_content($post->ID) ;
if(empty($st_page_options['page_options']['show_content']) ||  $st_page_options['page_options']['show_content']==''){
        $showContent =  false;
}
?>

<div <?php post_class('text-content'); ?>>
<?php 
if($builder_content==''){
     echo '<div class="b40">';
      the_content();
      echo '</div>';
}else{
     echo do_shortcode($builder_content) ; 
}
?>
</div>
