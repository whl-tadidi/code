<?php 
/**
 * FULL layout no sidebar
 */ 
 
 

?>

<div class="main-outer-wrapper">
   <div class="main-wrapper container">
      <div class="row righ-sidebar">
        <?php 
       /**
         * @hooked st_page_title_tempalte();
         */ 
        do_action('st_top_page_template'); 
        ?>

        <div class="content-wrapper twelve  columns b0">
         <?php
             /**
             * @hooked st_page_template();
             */ 
              do_action('st_page_template');
         ?>
         </div>
        
         
        <div class="clear"></div>
         <?php do_action('st_bottom_page_template'); ?>
        <div class="clear"></div>

        </div>
    </div><!-- main-wrapper  -->
     <?php do_action('st_bottom_main_wrapper'); ?>
</div><!-- END .main-outer-wrapper  -->

                              