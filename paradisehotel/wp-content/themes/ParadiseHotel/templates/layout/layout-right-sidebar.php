<?php 
/**
 * Right Sidebar layout
 */ 
 
  if(is_singular()):
        global $post;
        $st_page_builder = get_page_builder_options($post->ID);;
    else :
        $st_page_builder = array();  
    endif;
    
    if(!isset($st_page_builder['right_sidebar']))
        $st_page_builder['right_sidebar'] ='';
  
?>
<div class="main-outer-wrapper">
   <div class="main-wrapper container">
      <div class="row righ-sidebar-layout">
        <?php 
       /**
         * @hooked st_page_title_tempalte();
         */ 
        do_action('st_top_page_template'); 
        ?>
        
        <div class="content-wrapper eight  columns b0">
         <?php
             /**
             * @hooked st_page_template();
             */ 
              do_action('st_page_template');
         ?>
         </div>
        
          <div class="right-sidebar-wrapper four  columns omega">
            <div class="right-sidebar sidebar">
                <?php 
                  do_action('st_sidebar',$st_page_builder['right_sidebar'],'right');
                ?>
                <div class="clear"></div>
            </div>
            </div>

            <div class="clear"></div>
             <?php do_action('st_bottom_page_template'); ?>
            <div class="clear"></div>

        </div>
    </div><!-- main-wrapper  -->
     <?php do_action('st_bottom_main_wrapper'); ?>
</div><!-- END .main-outer-wrapper  -->




