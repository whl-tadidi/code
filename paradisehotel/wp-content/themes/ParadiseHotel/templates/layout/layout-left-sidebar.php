<?php 
/**
 * Left sidebar  layout
 */ 
 
 if(is_singular()):
        global $post;
          $st_page_builder =  get_page_builder_options($post->ID);
    else :
          $st_page_builder = array();  
    endif;
    
     if(!isset($st_page_builder['left_sidebar']))
        $st_page_builder['left_sidebar'] ='';
?>


<div class="main-outer-wrapper">
   <div class="main-wrapper container">
      <div class="row left-sidebar-layout">
        <?php 
       /**
         * @hooked st_page_title_tempalte();
         */ 
        do_action('st_top_page_template'); 
        ?>
        
         <div class="left-sidebar-wrapper four  columns omega">
            <div class="left-sidebar sidebar">
                <?php 
                    do_action('st_sidebar',$st_page_builder['left_sidebar'],'left');
                ?>
                <div class="clear"></div>
            </div>
         </div>

        
        <div class="content-wrapper eight  columns b0">
         <?php
             /**
             * @hooked st_page_template();
             */ 
              do_action('st_page_template');
         ?>
         </div>
        
         
        <div class="clear"></div>
         <?php do_action('st_bottom_page_template'); ?>
        <div class="clear"></div>

        </div>
    </div><!-- main-wrapper  -->
     <?php do_action('st_bottom_main_wrapper'); ?>
</div><!-- END .main-outer-wrapper  -->


