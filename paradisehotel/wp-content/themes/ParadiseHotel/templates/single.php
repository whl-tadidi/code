 <?php
 global $post, $page;
  the_post(); 
 $st_page_options =  $st_page_builder = get_page_builder_options($post->ID);
 $builder_content = get_page_builder_content($post->ID); 
 $date_format = get_option('date_format');
 
 global $authordata;
         
 $author_link = sprintf(  __('Posted by <a href="%1$s" title="%2$s" rel="author">%3$s</a>'),
            esc_url( get_author_posts_url( $authordata->ID, $authordata->user_nicename ) ),
            esc_attr( sprintf( __( 'Posted by %s' ,'smooththemes'), get_the_author() ) ),
             get_the_author() 
        );
 
 
 ?>
  
  <div class="news-title">
          <h3><?php the_title(); ?></h3>
          <?php if(st_get_setting("s_show_post_meta")!='n'){ ?>
                <ul>
                    <li class="date"><?php printf('Posted on %s',get_the_time($date_format)); ?></li>
                    <li class="auth"><?php echo $author_link;  ?></li>
                    <li class="comments"><?php comments_number(__('0 Comment','smooththemes'),__('1 Comment','smooththemes'),__('% Comments','smooththemes') ); ?> </li>
                    <li class="category"><?php the_category(', '); ?></li>
                </ul>
              <br class="clear">
           <?php } ?>   
    </div>

    <div class="page-content">
    
     <?php 
     if(st_get_setting('s_show_featured_img','y')!='n'){
     $thumb_html = st_post_thumbnail($post->ID,'st_large',false, true);
        if($page<2 && $thumb_html!=''): 
        ?>
        <div class="page-featured-image cpt-thumb-wrapper">
            <?php  echo $thumb_html;?>
        </div>
      <?php endif; 
      }
      
       ?>
                                              
        <?php 
        do_action('st_before_the_content',$post);
         echo '<div class="text-content">';
            the_content(); 
         echo '</div>';
        do_action('st_after_the_content',$post);
        do_action('st_after_page_content');
        
            $args = array(
              'before'           => '<p class="single-pagination">' . __('Pages:','smooththemes'),
              'after'            => '</p>',
              'link_before'      => '',
              'link_after'       => '',
              'next_or_number'   => 'number',
              'nextpagelink'     => __('Next page','smooththemes'),
              'previouspagelink' => __('Previous page','smooththemes'),
              'pagelink'         => '%',
              'echo'             => 1
            );
            
            wp_link_pages( $args ); 
                    
         ?> 
        
        
        <div class="clear"></div>
    </div><!-- END page-content-->
    
    <div class="page-single-element">
    
    <?php  if(st_get_setting('s_show_post_tag','y')!='n'){ ?>
        <p class="page-tags">
            <?php the_tags('<b>'.__('Tags:','smooththemes').'</b> ',', ',''); ?>
        </p>
        <div class="clear"></div>
    <?php } ?>

     <?php  if(st_get_setting('s_show_comments','y')!='n'){ ?>
      <?php comments_template('', true ); ?>
      <?php } ?>
      
      
    </div><!-- Single Page Element -->