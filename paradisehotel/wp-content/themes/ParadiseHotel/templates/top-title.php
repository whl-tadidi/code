<?php 
 global  $post;
$title  = $tagline='';
$is_slider=  false;
if(is_singular()){
    

    if(is_page()){
       
        $st_page_options =  $st_page_builder = get_page_builder_options($post->ID);
        if(empty($st_page_options) || (isset($st_page_options['show_title'])  &&  $st_page_options['show_title']==1)){
             // show title
              $title  = get_the_title($post->ID);
              $tagline =  isset($st_page_options['tagline']) ? esc_html($st_page_options['tagline']) : false;
        }else{
            $title= '';
        }
        
        if(isset($st_page_builder['show_top_slider'])  && $st_page_builder['show_top_slider']==1){
             st_top_slider($post->ID,'b0');
             $is_slider = true;
        }
            
    }elseif(is_singular('room')){
        $title  = get_the_title($post->ID);
        $tagline=  get_post_meta($post->ID,'_room_tagline',true);
    }else{
        
         if(st_get_setting('show_blog_toptitle','y')!='n'){
           $title  =   st_get_setting('blog_toptitle','');
           $tagline  =   st_get_setting('blog_tagline','');
        }
    }
    
       
}elseif(is_author()){
     $title = get_the_author();
}elseif(is_tax() || is_category() || is_tag()){
      $title = single_term_title('',false);
      $tagline = strip_tags(category_description());
}elseif(is_search()){
    $title = sprintf(__('Seach for: <i class="serch-text">%s</i>','smooththemes'), esc_html( get_search_query() ) ) ;
}elseif( (is_archive() || is_day() || is_date() || is_month() || is_year() || is_time()) && !is_category() ){
    
   if ( is_day() ) : 
      $title =  sprintf( __( 'Daily Archives: %s', 'smooththemes' ), '<i class="archive-t">' . get_the_date() . '</i>' ); 
     elseif ( is_month() ) : 
        $title =     sprintf( __( 'Monthly Archives: %s', 'smooththemes' ), '<i class="archive-t">' . get_the_date( _x( 'F Y', 'monthly archives date format', 'smooththemes' ) ) . '</i>' );
     elseif ( is_year() ) : 
        $title =     sprintf( __( 'Yearly Archives: %s', 'smooththemes'), '<i class="archive-t">' . get_the_date( _x( 'Y', 'yearly archives date format', 'smooththemes' ) ) . '</i>' ); 
     else : 
        $title =__( 'Blog Archives', 'smooththemes' );
    endif; 
    
}elseif(is_404()){
    $title =__('404','smooththemes');
    
}elseif((is_home() || is_front_page()) && !is_page()){  // default if user do not select static page

    if(st_get_setting('show_blog_toptitle','y')!='n'){
           $title  =   st_get_setting('blog_toptitle','');
           $tagline  =   st_get_setting('blog_tagline','');
    }
        
    

}



if(st_is_woocommerce() && is_woocommerce()){
        
        $post_id = st_get_shop_page();
    
        $st_page_options =  $st_page_builder = get_page_builder_options($post_id);
        if(empty($st_page_options) || (isset($st_page_options['show_title'])  &&  $st_page_options['show_title']==1)){
             // show title
              
              $title  = get_the_title($post_id);
              $tagline =  isset($st_page_options['tagline']) ? esc_html($st_page_options['tagline']) : false;
        }else{
            $title= '';
        }
        
        if(isset($st_page_builder['show_top_slider'])  && $st_page_builder['show_top_slider']==1){
              $is_slider = true;
             st_top_slider($post_id,'b0');
        }
    }




if($tagline !='' ||  $title!='' || is_singular('event')){
    
    if(is_singular('event')){
        
        $start_date = get_post_meta($post->ID,'_st_event_start_date',true);
    
        if($start_date!=''){
            $start_date = strtotime($start_date);
        }
        
        $end_date = get_post_meta($post->ID,'_st_event_end_date',true);
        if($end_date!=''){
            $end_date = strtotime($end_date);
        }
        
      //  $address = get_post_meta($post->ID,'_st_event_meta_address',true);
        $price = get_post_meta($post->ID,'_st_event_meta_price',true);
        $map =  get_post_meta($post->ID,'_st_event_meta_map',true);
         if(!array($map)){
             $map =  array('address'=>'');
         }
         $address =  $map['address'];
     
     
        $html_map = st_gmap_template($map);
       
 
        ?>
        
        <div class="twelve column top-event-title  b0<?php echo $is_slider ? ' slider-above' :' no-slider'; ?>">
              <div class="event-detail">
                   <div class="event-data">
                        <strong> <a href="<?php echo $link; ?>"><?php echo date_i18n('d',$start_date); ?></a></strong><span><?php echo date_i18n('M',$start_date); ?></span>
                  </div>
                  <h1><?php echo get_the_title($post->ID); ?>
                      <?php echo $tagline!='' ? ' <span>'.$tagline.'</span> ': ''; ?>
                  </h1>
                        <ul>
                            <?php if($address){ ?>
                            <li class="map-marker"><?php echo $address; ?></span></li>
                            <?php } // end addres ?>
                            <li class="time"><?php _e(sprintf(__('Start at %s','smooththemes'),date_i18n('H:iA',$start_date))) ?></li>
                            <li class="price"><?php echo $price; ?></li>
                        </ul>
                      <br class="clear">
                  </div>
          </div>
          
          <?php if($html_map!=''){ ?>
          <div class="twelve column">
                <?php echo $html_map; ?> 
          </div>
          <?php } ?>
          
        
          
        <?php 

         
    }else{
         ?>
        <div class="twelve column top-title  b0<?php echo $is_slider ? ' slider-above' :' no-slider'; ?>">
                <h1 class="page-title"><?php echo $title  ?>
                     <?php echo $tagline!='' ? ' <span>'.$tagline.'</span> ': ''; ?>
                </h1>
          </div>
        <?php 
         
    }

}else{
?>
<div class="no-toptitle"></div>
<?php
}

