<?php


class ratings {

    private $widget_id;
    private $data = array();
    
    
function __construct($wid) {
    $this->widget_id = $wid;
    $this->data =  get_post_meta($wid,'_st_ratings_data',true);
   // $this->data = array();
    if(empty($this->data) ||  !isset($this->data['number_votes']) ){
        $this->data = array();
        $this->data['number_votes'] =  0;
        $this->data['total_points'] =  0;
        $this->data['whole_avg'] = 0;
        $this->data['dec_avg'] = '0.0';
    }
    
     $this->data['vote_txt'] = sprintf(__('%1$s votes (%2$s Rating)','smoothethemes') , $this->data['number_votes'], $this->data['dec_avg'] );
    
}
public function get_ratings() {
    echo json_encode($this->data);
}
public function vote() {
    
    # Get the value of the vote
    preg_match('/star_([1-5]{1})/', $_POST['clicked_on'], $match);
    $vote = $match[1];
    
    $ID = $this->widget_id;
    # Update the record if it exists
    if(!empty($this->data)) {
        $this->data['number_votes'] += 1;
        $this->data['total_points'] += $vote;
    }
    # Create a new one if it doesn't
    else {
        $this->data['number_votes'] = 1;
        $this->data['total_points'] = $vote;
    }
    
    $this->data['dec_avg'] = round( $this->data['total_points'] / $this->data['number_votes'], 1 );
    $this->data['whole_avg'] = round( $this->data['dec_avg'] );
    
        

     update_post_meta($ID,'_st_ratings_data',$this->data);
     update_post_meta($ID,'_st_ratings_avg',$this->data['whole_avg']);
     update_post_meta($ID,'_st_ratings_total_points',$this->data['total_points']);
     update_post_meta($ID,'_st_ratings_number_votes',$this->data['number_votes']);
    
    $this->get_ratings();
}

# ---
# end class
}
