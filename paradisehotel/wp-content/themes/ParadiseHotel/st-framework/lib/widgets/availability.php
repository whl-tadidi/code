<?php


class STAvailability extends WP_Widget {
         function STAvailability() {
             $widget_ops = array('classname' => 'Reservation form', );
             $this->WP_Widget('STAvailability', 'ST Reservation form', $widget_ops);
         }
          
         function widget($args, $instance) {
            // prints the widget
            extract($args);
            
             $defaults_setting = array(
                'title' => '',
                'before_form' => '',
                'mail_to'=>'',
                'after_form' => ''
            );
            $instance = wp_parse_args(  $instance, $defaults_setting );
            
            //variables from widget setting
            $title = apply_filters('widget_title', $instance['title']);
            $settings = $instance;
            
            if($postcount<=0){
                $postcount =4;
            }

            // Before Widget :
            echo $before_widget;

              
            
            include(ST_TEMPLATE_DIR.'/forms/availability.php');
             
            
            // After Widget
            echo $after_widget;
         }
          
         function update($new_instance, $old_instance) {
            //save the widget
            $instance = $old_instance;
            
            // Only text inputs
            $instance['title'] = strip_tags($new_instance['title']);
            $instance['mail_to'] = $new_instance['mail_to'];
    		$instance['before_form'] = $new_instance['before_form'];
    		$instance['after_form']  = $new_instance['after_form'];

            return $instance;
         }
          
         function form($instance) {
            //widgetform in backend
                
            //default widget settings.
            $defaults_setting = array(
                'title' => '',
                'before_form' => '',
                'mail_to'=>'',
                'after_form' => ''
            );
            $instance = wp_parse_args(  $instance, $defaults_setting );
            ?>
            
            <p>
                <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e('Title:','smooththemes') ?></label>
			    <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance['title']; ?>" />
            </p>
            
            <p>
                <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e('Mail to:','smooththemes') ?></label>
                <input class="widefat" id="<?php echo $this->get_field_id( 'mail_to' ); ?>" name="<?php echo $this->get_field_name( 'mail_to' ); ?>" value="<?php echo $instance['mail_to']; ?>" />
            </p>
            
            <p>
                <label for="<?php echo $this->get_field_id( 'before_form' ); ?>"><?php _e('Before form:','smooththemes') ?></label>
                <textarea rows="7" class="widefat" id="<?php echo $this->get_field_id( 'before_form' ); ?>" name="<?php echo $this->get_field_name( 'before_form' ); ?>" ><?php echo esc_html($instance['before_form']); ?></textarea>
            </p>
            
            <p>
                <label for="<?php echo $this->get_field_id( 'after_form' ); ?>"><?php _e('After form:','smooththemes') ?></label>
                <textarea rows="7" class="widefat" id="<?php echo $this->get_field_id( 'after_form' ); ?>" name="<?php echo $this->get_field_name( 'after_form' ); ?>" ><?php echo esc_html($instance['after_form']); ?></textarea>
            </p>

         <?php   
         }
     }
     
    
register_widget( 'STAvailability' );