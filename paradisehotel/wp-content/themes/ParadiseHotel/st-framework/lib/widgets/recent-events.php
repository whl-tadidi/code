<?php 

class STRecentEvents extends WP_Widget {

	public function __construct() {
		// widget actual processes
        parent::__construct(
	 		'strecentevents', // Base ID
			__('ST Recent Events','smooththemes'), // Name
			array( 'description' => __( 'Display Recent Events', 'smooththemes' ), ) // Args
		);
	}

 	public function form( $instance ) {
		// outputs the options form on admin
        
        if ( isset( $instance[ 'title' ] ) ) {
			$title = $instance[ 'title' ];
		}
		else {
			$title = '';
		}
        
        $number = intval($instance[ 'number' ]);
        
        if($number<=0){
            $number = 3; // default  = 3;
        }
        
		?>
		<p>
		<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:','smooththemes'); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
		</p>
        
        	<p>
    		<label for="<?php echo $this->get_field_id( 'number' ); ?>"><?php echo __('How many posts to show ? ' ,'smooththemes') ?></label> 
    		<input class="widefat" id="<?php echo $this->get_field_id( 'number' ); ?>" name="<?php echo $this->get_field_name( 'number' ); ?>" type="text" value="<?php echo esc_attr( $number ); ?>" />
    		</p>
        
		<?php 
	}

	public function update( $new_instance, $old_instance ) {
		// processes widget options to be saved
        $instance = array();
		$instance['title'] = strip_tags( $new_instance['title'] );
        $instance[ 'number' ] = intval($new_instance[ 'number' ]);
		return $instance;
	}

	public function widget( $args, $instance ) {
		// outputs the content of the widget
            global $wpdb,$post;
            
            $date_format = get_option('date_format','M j, Y');
            
            extract( $args );
    		$title = apply_filters( 'widget_title', $instance['title'] );
            $number = intval($instance['number'] );
            if($number<=0){
                $number = 3; // default  = 3;
            }
    
    		echo $before_widget;
    		if ( ! empty( $title ) )
    			echo $before_title . $title . $after_title;

               $posts =  st_get_recent_events($number);
                
      

        	if($posts){ ?>
            <ul class="st-recent-events" id="recent-event-list">
                <?php 
                $i = 0;
                foreach($posts as $post){
                    
                    $start_date = get_post_meta($post->ID,'_st_event_start_date',true);
        
                if($start_date!=''){
                    $start_date = strtotime($start_date);
                }
                
                $end_date = get_post_meta($post->ID,'_st_event_end_date',true);
                if($end_date!=''){
                    $end_date = strtotime($end_date);
                }
                 
                $link = get_permalink($post->ID);
                     
                setup_postdata($post); ?>
                <li class="widget-event-wrapper<?php echo $class; ?>">
                
                     <?php  echo st_post_small_thumbnail($post->ID); ?>
                    
                    <p><a href="<?php echo $link ; ?>"><?php the_title(); ?></a> </p>
                     <span class="recent-date"><?php echo date_i18n($date_format,$start_date); ?></span><br class="clear"></li>
                <?php 
                
                } ?>
             </ul>
            <?php }	wp_reset_query() ;
            
        	echo $after_widget;
	}

}

register_widget( 'STRecentEvents' );


