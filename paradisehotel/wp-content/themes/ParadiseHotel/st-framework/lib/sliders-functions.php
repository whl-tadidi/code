<?php 

/**
 * Display slider as type
 * 
 *  'layerslider'=>"Layer slider",
    'revslider'=>'Revolution Slider',
    'elasticslideshow'=>'Elastic Slide show', 
    'nivo'=>'Nivo Slider',
    'flexslider'=>'Flex Slider'
 * 
 */
function st_the_slider($page_builder_data, $must_show = false){
    // if empty data 
    
     if( (boolean) $must_show== false){
        if( (!isset($page_builder_data['show_top_slider'])  || $page_builder_data['show_top_slider']!=1) &&  (!isset($page_builder_data['show_slider']) || $page_builder_data['show_slider']!=1 ) ){
         return ;
        }
     }
     
    $img_size = (isset($page_builder_data['size'])  && $page_builder_data['size']!='' ) ? $page_builder_data['size']  : false;
    
     $img_size = ($img_size) ?  $img_size : 'st_large';
     
     if(!isset($page_builder_data['slider_type'])){
         $page_builder_data['slider_type'] ='';
     }

    switch(strtolower($page_builder_data['slider_type'])){
         
          
           default:
            
                $settings['show_caption']='yes';
               
                $data= $page_builder_data;
                if(is_file(ST_TEMPLATE_DIR.'/sliders/flex.php')){
                    include(ST_TEMPLATE_DIR.'/sliders/flex.php');
                }
                
               
          break;
          
           
    }
    
    
}
