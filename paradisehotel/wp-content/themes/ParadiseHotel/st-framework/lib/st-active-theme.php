<?php
if ( is_admin() && isset($_GET['activated'] ) && $pagenow == 'themes.php' ) {
      // your code here
      st_theme_activate();
}

function st_theme_activate() {
   // update_option(ST_SETTINGS_OPTION,$options);
    st_update_default_settings(true);
    ob_start();
  //  header('Location: '.admin_url('admin.php?page='.ST_PAGE_SLUG));
}



/**
 * Update to default settings
 */ 
function  st_update_default_settings($check  = false){
    $option_name = '_'.ST_NAME.'_is_import_default';
    
    if($check === true){
        if(get_option($option_name)=='y'){
              return false;
        }
    }
    
    // default setting options    
    $default ='a:47:{s:6:"layout";s:1:"2";s:9:"site_logo";s:86:"http://demo.smooththemes.com/paradise/wp-content/themes/paradise/assets/img/logo_1.png";s:12:"site_favicon";s:63:"http://localhost/paradise/wp-content/uploads/2013/05/dinner.png";s:8:"sidebars";a:7:{i:0;a:2:{s:5:"title";s:6:"Events";s:2:"id";s:15:"id1369276145066";}i:1;a:2:{s:5:"title";s:7:"Contact";s:2:"id";s:15:"id1363686920866";}i:2;a:2:{s:5:"title";s:5:"Rooms";s:2:"id";s:15:"id1366601756671";}i:3;a:2:{s:5:"title";s:5:"About";s:2:"id";s:15:"id1369379088994";}i:4;a:2:{s:5:"title";s:3:"Ads";s:2:"id";s:15:"id1366345831849";}i:5;a:2:{s:5:"title";s:12:"Widget Check";s:2:"id";s:15:"id1366514700885";}i:6;a:2:{s:5:"title";s:7:"Product";s:2:"id";s:15:"id1369470811833";}}s:9:"body_font";a:8:{s:9:"font-size";s:2:"14";s:14:"font-size-unit";s:2:"px";s:11:"line-height";s:2:"22";s:16:"line-height-unit";s:2:"px";s:5:"color";s:6:"444444";s:11:"font-family";s:19:"PTSansNarrowRegular";s:10:"font-style";s:6:"normal";s:11:"font-weight";s:6:"normal";}s:13:"headings_font";a:2:{s:5:"color";s:6:"3b576b";s:11:"font-family";s:19:"PTSansNarrowRegular";}s:9:"heading_1";a:2:{s:9:"font-size";s:2:"24";s:14:"font-size-unit";s:2:"px";}s:9:"heading_2";a:2:{s:9:"font-size";s:2:"20";s:14:"font-size-unit";s:2:"px";}s:9:"heading_3";a:2:{s:9:"font-size";s:2:"16";s:14:"font-size-unit";s:2:"px";}s:9:"heading_4";a:2:{s:9:"font-size";s:2:"18";s:14:"font-size-unit";s:2:"px";}s:9:"heading_5";a:2:{s:9:"font-size";s:2:"12";s:14:"font-size-unit";s:2:"px";}s:9:"heading_6";a:2:{s:9:"font-size";s:2:"12";s:14:"font-size-unit";s:2:"px";}s:16:"page_title_color";s:6:"444444";s:12:"page_tagline";s:6:"999999";s:10:"link_color";s:6:"3B576B";s:16:"link_color_hover";s:6:"3B576B";s:9:"btn_color";s:6:"444444";s:15:"btn_color_hover";s:6:"444444";s:18:"show_blog_toptitle";s:1:"y";s:13:"blog_toptitle";s:11:"New & Pomos";s:12:"blog_tagline";s:72:"Lorem ipsum dolor sit amet, per ne purto corpora, cu eam cetero dolores.";s:19:"s_show_featured_img";s:1:"y";s:16:"s_show_post_meta";s:1:"y";s:15:"s_show_post_tag";s:1:"y";s:15:"s_show_comments";s:1:"y";s:16:"footer_copyright";s:216:"Copyright &copy; 2013 All Rights Reserved. Powered by <a title=\"WordPress\" href=\"http://wordpress.org\">WordPress</a> and <a title=\"Premium WordPress Themes\" href=\"http://www.smooththemes.com\">SmoothThemes</a>";s:9:"post_room";s:0:"";s:10:"post_event";s:0:"";s:13:"wheather_show";s:1:"y";s:14:"wheather_woeid";s:4:"4063";s:13:"wheather_unit";s:1:"f";s:14:"flex_animation";s:4:"fade";s:18:"flex_animationLoop";s:1:"y";s:14:"flex_slideshow";s:1:"y";s:19:"flex_slideshowSpeed";s:4:"7000";s:22:"flex_animationDuration";s:4:"7000";s:19:"flex_animationSpeed";s:3:"400";s:18:"flex_pauseOnAction";s:5:"false";s:17:"flex_pauseOnHover";s:1:"n";s:15:"flex_controlNav";s:1:"y";s:14:"flex_randomize";s:1:"n";s:12:"fsc_autoplay";s:1:"y";s:13:"fsc_slideshow";s:1:"y";s:12:"fsc_interval";s:4:"5000";s:20:"fsc_transition_speed";s:3:"700";s:21:"headder_tracking_code";s:0:"";s:20:"footer_tracking_code";s:0:"";}';
    
    $translate  = 'YToxMDg6e3M6Njk6IlRoaXMgcG9zdCBpcyBwYXNzd29yZCBwcm90ZWN0ZWQuIEVudGVyIHRoZSBwYXNzd29yZCB0byB2aWV3IGNvbW1lbnRzLiI7czowOiIiO3M6MTI6Ik5vIFJlc3BvbnNlcyI7czowOiIiO3M6MTI6Ik9uZSBSZXNwb25zZSI7czowOiIiO3M6MTE6IiUgUmVzcG9uc2VzIjtzOjA6IiI7czoxNDoiT2xkZXIgQ29tbWVudHMiO3M6MDoiIjtzOjE0OiJOZXdlciBDb21tZW50cyI7czowOiIiO3M6MjoidG8iO3M6MDoiIjtzOjIwOiJDb21tZW50cyBhcmUgY2xvc2VkLiI7czowOiIiO3M6MTk6IkxlYXZlIGEgUmVwbHkgdG8gJXMiO3M6MDoiIjtzOjExOiJZb3UgbXVzdCBiZSI7czowOiIiO3M6MTg6InRvIHBvc3QgYSBjb21tZW50LiI7czowOiIiO3M6Mjk6IlJlcXVpcmVkIGZpZWxkcyBhcmUgbWFya2VkICVzIjtzOjA6IiI7czoxMzoiTGVhdmUgYSBSZXBseSI7czowOiIiO3M6MTI6IkNhbmNlbCBSZXBseSI7czowOiIiO3M6MTI6IlBvc3QgQ29tbWVudCI7czowOiIiO3M6NzoiQ29tbWVudCI7czowOiIiO3M6NTc6IllvdSBtdXN0IGJlIDxhIGhyZWY9IiVzIj5sb2dnZWQgaW48L2E+IHRvIHBvc3QgYSBjb21tZW50LiI7czowOiIiO3M6NDE6IllvdXIgZW1haWwgYWRkcmVzcyB3aWxsIG5vdCBiZSBwdWJsaXNoZWQuIjtzOjA6IiI7czo0OiJOYW1lIjtzOjA6IiI7czo1OiJFbWFpbCI7czowOiIiO3M6NzoiV2Vic2l0ZSI7czowOiIiO3M6NzoiUGFnZSAlcyI7czowOiIiO3M6NjoiU2VhcmNoIjtzOjA6IiI7czoxNToiUGVybWFsaW5rIHRvICVzIjtzOjA6IiI7czoxMjoiT3BlbiBHYWxsZXJ5IjtzOjA6IiI7czoyNDoiJTEkcyB2b3RlcyAoJTIkcyBSYXRpbmcpIjtzOjA6IiI7czozOiJBbGwiO3M6MDoiIjtzOjI6IsOXIjtzOjA6IiI7czo5OiJSZWFkIG1vcmUiO3M6MDoiIjtzOjI6ImF0IjtzOjA6IiI7czoxODoiSW52YWxpZCBSZWNpcGllbnRzIjtzOjA6IiI7czoxODoiSW52YWxpZCB5b3VyIEVtYWlsIjtzOjA6IiI7czo4OiJOYW1lOiAlcyI7czowOiIiO3M6OToiRW1haWw6ICVzIjtzOjA6IiI7czoxMzoiUm9vbSBUeXBlOiAlcyI7czowOiIiO3M6MTY6Ik5vLiBvZiBndWVzdDogJXMiO3M6MDoiIjtzOjEyOiJDaGVjayBpbjogJXMiO3M6MDoiIjtzOjEzOiJDaGVjayBvdXQ6ICVzIjtzOjA6IiI7czozNDoiWW91IGhhdmUgYSBuZXcgcmVzZXJ2YXRpb24gZnJvbSAlcyI7czowOiIiO3M6MzU6IllvdSBoYXZlIGEgbmV3IHJlc2VydmF0aW9uIHJlcXVlc3QhIjtzOjA6IiI7czoxODoiQ2FuIG5vdCBzZW5kIG1haWwuIjtzOjA6IiI7czoxMDoiRW1haWw6ICAlcyI7czowOiIiO3M6MTE6Ik1lc3NhZ2U6ICVzIjtzOjA6IiI7czoyODoiWW91IGhhdmUgYSBuZXcgZW1haWwgZnJvbSAlcyI7czowOiIiO3M6MjM6IllvdSBoYXZlIGEgbmV3IG1lc3NhZ2UhIjtzOjA6IiI7czo0NzoiQSB3aWRnZXQgdGhhdCBhbGxvd3MgdGhlIGRpc3BsYXkgb2YgYWRzIDEyNXgxMjUiO3M6MDoiIjtzOjM0OiJEaXNwbGF5IFJlY2VudCBDb21tZW50IHdpdGggYXZhdGFyIjtzOjA6IiI7czoyODoiSG93IG1hbnkgY29tbWVudHMgdG8gc2hvdyA/ICI7czowOiIiO3M6MjE6IkRpc3BsYXkgUmVjZW50IEV2ZW50cyI7czowOiIiO3M6MjA6IkRpc3BsYXkgUmVjZW50IFBvc3RzIjtzOjA6IiI7czoyNToiRGlzcGxheSBSZWxhdGVkIFBvcnRmb2xpbyI7czowOiIiO3M6OToiMCBDb21tZW50IjtzOjA6IiI7czo5OiIxIENvbW1lbnQiO3M6MDoiIjtzOjEwOiIlIENvbW1lbnRzIjtzOjA6IiI7czoxMDoiU1QgVHdpdHRlciI7czowOiIiO3M6MjM6IlRoaXMgZmllbGQgaXMgcmVxdWlyZWQuIjtzOjA6IiI7czoyMjoiUGxlYXNlIGZpeCB0aGlzIGZpZWxkLiI7czowOiIiO3M6MzU6IlBsZWFzZSBlbnRlciBhIHZhbGlkIGVtYWlsIGFkZHJlc3MuIjtzOjA6IiI7czoyNToiUGxlYXNlIGVudGVyIGEgdmFsaWQgVVJMLiI7czowOiIiO3M6MjY6IlBsZWFzZSBlbnRlciBhIHZhbGlkIGRhdGUuIjtzOjA6IiI7czozMjoiUGxlYXNlIGVudGVyIGEgdmFsaWQgZGF0ZSAoSVNPKS4iO3M6MDoiIjtzOjI4OiJQbGVhc2UgZW50ZXIgYSB2YWxpZCBudW1iZXIuIjtzOjA6IiI7czoyNToiUGxlYXNlIGVudGVyIG9ubHkgZGlnaXRzLiI7czowOiIiO3M6NDA6IlBsZWFzZSBlbnRlciBhIHZhbGlkIGNyZWRpdCBjYXJkIG51bWJlci4iO3M6MDoiIjtzOjM0OiJQbGVhc2UgZW50ZXIgdGhlIHNhbWUgdmFsdWUgYWdhaW4uIjtzOjA6IiI7czo0NDoiUGxlYXNlIGVudGVyIGEgdmFsdWUgd2l0aCBhIHZhbGlkIGV4dGVuc2lvbi4iO3M6MDoiIjtzOjQxOiJQbGVhc2UgZW50ZXIgbm8gbW9yZSB0aGFuIHswfSBjaGFyYWN0ZXJzLiI7czowOiIiO3M6Mzc6IlBsZWFzZSBlbnRlciBhdCBsZWFzdCB7MH0gY2hhcmFjdGVycy4iO3M6MDoiIjtzOjU3OiJQbGVhc2UgZW50ZXIgYSB2YWx1ZSBiZXR3ZWVuIHswfSBhbmQgezF9IGNoYXJhY3RlcnMgbG9uZy4iO3M6MDoiIjtzOjQxOiJQbGVhc2UgZW50ZXIgYSB2YWx1ZSBiZXR3ZWVuIHswfSBhbmQgezF9LiI7czowOiIiO3M6NDc6IlBsZWFzZSBlbnRlciBhIHZhbHVlIGxlc3MgdGhhbiBvciBlcXVhbCB0byB7MH0uIjtzOjA6IiI7czo1MDoiUGxlYXNlIGVudGVyIGEgdmFsdWUgZ3JlYXRlciB0aGFuIG9yIGVxdWFsIHRvIHswfS4iO3M6MDoiIjtzOjE4OiJQcmltYXJ5IE5hdmlnYXRpb24iO3M6MDoiIjtzOjE3OiJGb290ZXIgTmF2aWdhdGlvbiI7czowOiIiO2k6NDA0O3M6MDoiIjtzOjI0OiJHbyBiYWNrIHRvIHByZXZpb3VzIHBhZ2UiO3M6MDoiIjtzOjE0OiJHbyB0byBob21lcGFnZSI7czowOiIiO3M6NjoiUGFnZXM6IjtzOjA6IiI7czo5OiJOZXh0IHBhZ2UiO3M6MDoiIjtzOjEzOiJQcmV2aW91cyBwYWdlIjtzOjA6IiI7czo1OiJUYWdzOiI7czowOiIiO3M6NTk6IlBvc3RlZCBieSA8YSBocmVmPSIlMSRzIiB0aXRsZT0iJTIkcyIgcmVsPSJhdXRob3IiPiUzJHM8L2E+IjtzOjA6IiI7czoxMjoiUG9zdGVkIGJ5ICVzIjtzOjA6IiI7czoxMDoiTG9hZGluZy4uLiI7czowOiIiO3M6Mzk6IlNlYWNoIGZvcjogPGkgY2xhc3M9InNlcmNoLXRleHQiPiVzPC9pPiI7czowOiIiO3M6MTg6IkRhaWx5IEFyY2hpdmVzOiAlcyI7czowOiIiO3M6MjA6Ik1vbnRobHkgQXJjaGl2ZXM6ICVzIjtzOjA6IiI7czozOiJGIFkiO3M6MDoiIjtzOjE5OiJZZWFybHkgQXJjaGl2ZXM6ICVzIjtzOjA6IiI7czoxOiJZIjtzOjA6IiI7czoxMzoiQmxvZyBBcmNoaXZlcyI7czowOiIiO3M6MTE6IlN0YXJ0IGF0ICVzIjtzOjA6IiI7czo4OiJDaGVjayBpbiI7czowOiIiO3M6OToiQ2hlY2sgb3V0IjtzOjA6IiI7czoxMToiTsKwb2YgZ3Vlc3QiO3M6MDoiIjtzOjE3OiItIHBsZWFzZSBzZWxlY3QgLSI7czowOiIiO3M6OToiUm9vbSB0eXBlIjtzOjA6IiI7czo5OiJZb3VyIE5hbWUiO3M6MDoiIjtzOjEwOiJZb3VyIEVtYWlsIjtzOjA6IiI7czo2OiJTdWJtaXQiO3M6MDoiIjtzOjk6Ikxhc3QgTmFtZSI7czowOiIiO3M6NzoiTWVzc2FnZSI7czowOiIiO3M6MTI6IlNlbmQgTWVzc2FnZSI7czowOiIiO3M6MTM6IlJlYWQgbW9yZSDihpIiO3M6MDoiIjtzOjE0OiJSZWFkIG1vcmUg4oaSICI7czowOiIiO3M6MzI6Ik5vIFJlc3BvbnNlcyB0byAmIzgyMjA7JXMmIzgyMjE7IjtzOjA6IiI7czozMzoiT25lIFJlc3BvbnNlcyB0byAmIzgyMjA7JXMmIzgyMjE7IjtzOjA6IiI7czozNjoiJTEkcyBSZXNwb25zZXMgdG8gJiM4MjIwOyUyJHMmIzgyMjE7IjtzOjA6IiI7fQ==';
    
    $default = maybe_unserialize($default);
    $default['site_logo'] = st_img('logo_1.png');
    update_option(ST_SETTINGS_OPTION,$default);
    
    if(st_is_wpml()){
        $langs = icl_get_languages('skip_missing=0&orderby=KEY&order=asc');
        foreach($langs as $l){
           update_option(ST_SETTINGS_OPTION.'_'.$l['language_code'],$default);
        }
    }
    // update translate options
    update_option(ST_TRANSLATE_OPTION, unserialize(base64_decode($translate)));
    
    update_option($option_name,'y');
    
}

/* Flush rewrite rules for custom post types. */
add_action( 'after_switch_theme', 'st_flush_rewrite_rules' );

/* Flush your rewrite rules */
function st_flush_rewrite_rules() {
  flush_rewrite_rules();
}




