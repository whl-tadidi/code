<?php
global $wp_registered_sidebars, $predefined_colors;
$st_sidebars = $wp_registered_sidebars;
$general_tab = array();
$tpl_sidebars = array();
foreach($st_sidebars as $k=> $s){
    $tpl_sidebars[$s['id']] = $s['name'];
}

$general_tab_page = array(
        
        array(
            'name'=>'layout',
            'title'=>__('Default Layout','smooththemes'),
            'type' =>'layout',
            'multiple'=> false,
            'options'=>array(
              //  '4'=> ST_ADMIN_URL.'/images/layout/3.png',
                '3'=> ST_ADMIN_URL.'/images/layout/2.png',
                '2'=> ST_ADMIN_URL.'/images/layout/1.png',
                '1'=> ST_ADMIN_URL.'/images/layout/0.png',
            ),
            'default'=>'2',
            'desc'=>'',
            'desc_bottom'=>''
         )
         
          
);


$general_tab_logo = array(
    array(
        'name'        =>'site_logo',
        'title'       =>'Upload logo',
        'type'        =>'upload',
        'default'     =>st_img('logo.png'),
        'desc'        =>'',
        'desc_bottom' =>'Upload your custom logo.'
    )
);
$general_tab_favicon = array(
        array(
            'name'        =>'site_favicon',
            'title'       =>'Upload Favicon',
            'type'        =>'upload',
            'desc'        =>'',
            'desc_bottom' =>'Upload your custom favicon.'
        )
    );



$oe_single_post = array(
    
    array(
        'name'        =>'s_show_featured_img',
        'title'       =>__('Show Featured Image on single post','smooththemes'),
        'type'        =>'radio',
        'multiple'    => false,
        'options'     =>array('y'=>__('Yes'), 'n'=>__('No')),
        'default'     =>'y',
        'desc'        =>'',
        'desc_bottom' =>''
     ),

    array(
        'name'        =>'s_show_post_meta',
        'title'       =>__('Show post meta (author, date, categrories) on single post','smooththemes'),
        'type'        =>'radio',
        'multiple'    => false,
        'options'     =>array('y'=>__('Yes'), 'n'=>__('No')),
        'default'     =>'y',
        'desc'        =>'',
        'desc_bottom' =>''
     ),
     array(
        'name'        =>'s_show_post_tag',
        'title'       =>__('Show post tags on single post','smooththemes'),
        'type'        =>'radio',
        'multiple'    => false,
        'options'     =>array('y'=>__('Yes'), 'n'=>__('No')),
        'default'     =>'y',
        'desc'        =>'',
        'desc_bottom' =>''
     ),
     array(
        'name'        =>'s_show_comments',
        'title'       =>__('Show comments on single post','smooththemes'),
        'type'        =>'radio',
        'multiple'    => false,
        'options'     =>array('y'=>__('Yes'), 'n'=>__('No')),
        'default'     =>'y',
        'desc'        =>'',
        'desc_bottom' =>''
     )
);

$oe_footer_copyright = array(
    array(
        'name'        =>'footer_copyright',
        'title'       =>'Footer CopyRight Infomation',
        'type'        =>'textarea',
        'default'     =>'&copy; 2012. All Rights Reserved. Created with love by <a href="#">SmoothThemes.Com</a>',
        'desc'        =>'',
        'desc_bottom' =>''
    ), 
);



$elements_color = array(

   array(
      
        'title'       =>__('Page title &amp; tagline','smooththemes'),
        'type'        =>'heading'
        
    ),
    
    array(
        'name'        =>'page_title_color',
        'title'       =>__('Page title color','smooththemes'),
        'type'        =>'color',
        'default'     =>'444444',
        'desc'        =>'',
        'desc_bottom' =>''
    ),
    
    array(
        'name'        =>'page_tagline',
        'title'       =>__('Page tagline color','smooththemes'),
        'type'        =>'color',
        'default'     =>'444444',
        'desc'        =>'',
        'desc_bottom' =>''
    ),

    array(
      
        'title'       =>__('Link color','smooththemes'),
        'type'        =>'heading'
        
    ),

    array(
        'name'        =>'link_color',
        'title'       =>__('Link color','smooththemes'),
        'type'        =>'color',
        'default'     =>'006699',
        'desc'        =>'',
        'desc_bottom' =>''
    ),
    
    array(
        'name'        =>'link_color_hover',
        'title'       =>__('Link color when hover','smooththemes'),
        'type'        =>'color',
        'default'     =>'006699',
        'desc'        =>'',
        'desc_bottom' =>''
    ),
    
       array(
      
        'title'       =>__('Button label color','smooththemes'),
        'type'        =>'heading'
        
    ),

    array(
        'name'        =>'btn_color',
        'title'       =>__('Label color','smooththemes'),
        'type'        =>'color',
        'default'     =>'444444',
        'desc'        =>'',
        'desc_bottom' =>''
    ),
    
    array(
        'name'        =>'btn_color_hover',
        'title'       =>__('Label color when hover','smooththemes'),
        'type'        =>'color',
        'default'     =>'444444',
        'desc'        =>'',
        'desc_bottom' =>''
    )
    
    
);




$tab_flexslider = array(
  array(
        'type'=>'heading',
        'title'=>__('Flex Slider settings','smooththemes'),
     ),
//animation
    array(
        'name'=>'flex_animation',
        'type'=>'radio',
        'title'=>__('Animation','smooththemes'),
        'options'=>array('fade'=>__('fade'),'slide'=>__('slide')),
        'default'=>'fade'
     ),
     /*
     array(
            'name'=>'flex_directionNav',
            'type'=>'radio',
            'title'=>__('directionNav','smooththemes'),
            'options'=>array('y'=>__('Yes'),'n'=>__('No')),
            'default'=>'y',
            'desc'=>'Next & Prev navigation'
        ),
       */
     array(
        'name'=>'flex_animationLoop',
        'type'=>'radio',
        'title'=>__('Should the animation loop?','smooththemes'),
        'options'=>array('y'=>__('Yes'),'n'=>__('No')),
        'default'=>'y'
     ),
      array(
        'name'=>'flex_slideshow',
        'type'=>'radio',
        'title'=>__('Animate slider automatically','smooththemes'),
        'options'=>array('y'=>__('Yes'),'n'=>__('No')),
        'default'=>'y'
     ),
     array(
        'name'=>'flex_slideshowSpeed',
        'type'=>'text',
        'title'=>__('Slideshow Speed','smooththemes'),
        'default'=>'7000',
        'desc_bottom'=>__('Set the speed of the slideshow cycling, in milliseconds, default: 7000','smooththemes')
     ),
     array(
        'name'=>'flex_animationDuration',
        'type'=>'text',
        'title'=>__('Animation Duration Speed','smooththemes'),
        'default'=>'500',
        'desc_bottom'=>__('Set the speed of the slideshow cycling, in milliseconds, default: 7000','smooththemes')
     ),
     array(
        'name'=>'flex_animationSpeed',
        'type'=>'text',
        'title'=>__('Animation Speed','smooththemes'),
        'default'=>'6000',
        'desc_bottom'=>__('Set the speed of animations, in milliseconds, default: 6000','smooththemes')
     ),
     array(
        'name'=>'flex_pauseOnAction',
        'type'=>'radio',
        'title'=>__('Pause On Action','smooththemes'),
        'options'=>array('true'=>__('Yes'),'false'=>__('No')),
        'default'=>'y'
     ),
     array(
        'name'=>'flex_pauseOnHover',
        'type'=>'radio',
        'title'=>__('Pause On Hover','smooththemes'),
        'options'=>array('y'=>__('Yes'),'n'=>__('No')),
        'default'=>'y'
     ),
     array(
        'name'=>'flex_controlNav',
        'type'=>'radio',
        'title'=>__('Create navigation for paging control of each clide','smooththemes'),
        'options'=>array('y'=>__('Yes'),'n'=>__('No')),
        'default'=>'y'
     ),
      array(
        'name'=>'flex_randomize',
        'type'=>'radio',
        'title'=>__('Randomize slide order','smooththemes'),
        'options'=>array('y'=>__('Yes'),'n'=>__('No')),
        'default'=>'n'
     )
);



$tab_sceen_slider = array(
  array(
        'type'=>'heading',
        'title'=>__('Full Screen Slider settings','smooththemes'),
     ),
     array(
        'name'=>'fsc_autoplay',
        'type'=>'radio',
        'title'=>__('Should the animation loop?','smooththemes'),
        'options'=>array('y'=>__('Yes'),'n'=>__('No')),
        'default'=>'y'
     ),
      array(
        'name'=>'fsc_slideshow',
        'type'=>'radio',
        'title'=>__('Slideshow on/off','smooththemes'),
        'options'=>array('y'=>__('On'),'n'=>__('Off')),
        'default'=>'y'
     ),
     array(
        'name'=>'fsc_interval',
        'type'=>'text',
        'title'=>__('Slideshow Speed','smooththemes'),
        'default'=>'6000',
        'desc_bottom'=>__('Length between transitions, default: 6000','smooththemes')
     ),
     
     array(
        'name'=>'fsc_transition_speed',
        'type'=>'text',
        'title'=>__('Speed of transition','smooththemes'),
        'default'=>'800',
        'desc_bottom'=>__('Speed of transition, default: 800','smooththemes')
     )
     
     
    
);



// Font Style Tabs

$font_body = array(

    array(
        'name'        =>'body_font',
        'title'       =>__('Body Font','smooththemes'),
        'type'        =>'style',
        'function'  =>'st_settings_fonts',
        'default'     =>'',
        'previetxt'=>'Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                        Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s,
                        when an unknown printer took a galley of type and scrambled it to make a type specimen book. 
                        It has survived not only five centuries, but also the leap into electronic typesetting,
                        remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset 
                        sheets containing Lorem Ipsum passages,
                        and more recently with desktop publishing software like Aldus PageMaker 
                        including versions of Lorem Ipsum.',
        'options'=>array(
                'font-family'=>'Droid Sans',
                'color'=>'000000',
                'font-weight' =>'normal',
                'font-style'=>'nomal',
                'line-height'=>'18', // unit px
                'line-height-unit'=>'px',
                'font-size'=>'12',
                'font-size-unit'=>'px',
                'letter-spacing'=>'0',
                'letter-spacing-uni'=>'px'
            ),
        'desc'        =>__('','smooththemes'),
        'desc_bottom' =>''
    ),
    
);



$font_heading = array(

    array(
        'name'        =>'headings_font',
        'title'       =>__('Heading Font','smooththemes'),
        'type'        =>'style',
        'function'  =>'st_settings_fonts',
        'default'     =>'',
        'previetxt'=>'<div style="font-size: 18px; line-height: 30px;">Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                        Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s,
                        when an unknown printer took a galley of type and scrambled it to make a type specimen book. 
                        It has survived not only five centuries, but also the leap into electronic typesetting,
                        remaining essentially unchanged.</div>',
        'options'=>array(
                'font-family'=>'Droid Sans'
            ),
         'support'=>array('font_family','color'),
        'desc'        =>'',
        'desc_bottom' =>''
    ),

    array(
        'name'        =>'heading_1',
        'title'       =>__('H1','smooththemes'),
        'type'        =>'style',
        'function'  =>'st_settings_fonts',
        'default'     =>'',
        'previetxt'=>'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
        'support'=>array('font_size'),
        'options'=>array(
                'font-size'=>'34'
            ),
        'desc_bottom' =>''
    ),
    
    array(
        'name'        =>'heading_2',
        'title'       =>__('H2','smooththemes'),
        'type'        =>'style',
        'function'  =>'st_settings_fonts',
        'default'     =>'',
        'previetxt'=>'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
        
         'support'=>array('font_size'),
        'options'=>array(
                'font-size'=>'34'
            ),
        'desc_bottom' =>''
    ),
    
    array(
        'name'        =>'heading_3',
        'title'       =>__('H3','smooththemes'),
        'type'        =>'style',
        'function'  =>'st_settings_fonts',
        'default'     =>'',
        'previetxt'=>'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
        
         'support'=>array('font_size'),
         'options'=>array(
                'font-size'=>'34'
            ),
        'desc_bottom' =>''
    ),
    
    array(
        'name'        =>'heading_4',
        'title'       =>__('H4','smooththemes'),
        'type'        =>'style',
        'function'  =>'st_settings_fonts',
        'default'     =>'',
        'previetxt'=>'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
         'support'=>array('font_size'),
         'options'=>array(
                'font-size'=>'34'
            ),
        'desc_bottom' =>''
    ),
    
    array(
        'name'        =>'heading_5',
        'title'       =>__('H5','smooththemes'),
        'type'        =>'style',
        'function'  =>'st_settings_fonts',
        'default'     =>'',
        'previetxt'=>'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
        
         'support'=>array('font_size'),
         'options'=>array(
                'font-size'=>'34'
            ),
        'desc_bottom' =>''
    ),
    
    array(
        'name'        =>'heading_6',
        'title'       =>__('H6','smooththemes'),
        'type'        =>'style',
        'function'  =>'st_settings_fonts',
        'default'     =>'',
        'previetxt'=>'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
         'support'=>array('font_size'),
         'options'=>array(
                'font-size'=>'34'
            ),
        'desc_bottom' =>''
    ),

);



$sidebar_tab = array(
    array(
        'name'        =>'sidebars',
        'title'       =>'Sidebars',
        'type'        =>'ui',
        'default'     =>'',
        'support'    =>array('title','id'),
        'desc'        =>'',
        'desc_bottom' =>'Create custom sidebar.'
     )
);


global $st_hooks;

$ads_tab = array(
    array(
        'name'        =>'ads',
        'title'       =>'Site Ads Management',
        'type'        =>'ui',
        'default'     =>'',
        'support'    =>array('title','content','hook'),
        'hooks'=>$st_hooks,
        'desc'        =>'',
        'desc_bottom' =>''
     )
);



$tracking_code= array(
    array(
        'name'=>'headder_tracking_code',
        'type'=>'textarea',
        'title'=>__('Header tracking code','smooththemes'),
        'default'=>''
     ),
     array(
        'name'=>'footer_tracking_code',
        'type'=>'textarea',
        'title'=>__('Footer tracking code','smooththemes'),
        'default'=>''
     ),
);


$blog_tab = array(

    array(
        'name'        =>'show_blog_toptitle',
        'title'       =>__('Show Top title in Categories page and Single','smooththemes'),
        'options'=>array('y'=>__('Yes'),'n'=>__('No')),
        'default'=>'y',
        'type'=>'radio',
        'desc'        =>'',
        'desc_bottom' =>''
     ),
    
    array(
        'name'        =>'blog_toptitle',
        'title'       =>__('Top title','smooththemes'),
        'type'        =>'text',
        'default'     =>__('','smooththemes'),
        'desc'        =>'',
        'desc_bottom' =>''
     ),
     array(
        'name'        =>'blog_tagline',
        'title'       =>__('Top Tagline','smooththemes'),
        'type'        =>'text',
        'default'     =>__('','smooththemes'),
        'desc'        =>'',
        'desc_bottom' =>''
     )
     
     
);


/**
 * This options called in function  st_localize_script();
 * 
 */
$wheather_tab = array(

    array(
        'name'        =>'wheather_show',
        'title'       =>__('Show wheather','smooththemes'),
        'options'=>array('y'=>__('Yes'),'n'=>__('No')),
        'default'=>'y',
        'type'=>'radio',
        'desc'        =>'',
        'desc_bottom' =>''
     ),
    

    array(
        'name'        =>'wheather_woeid',
        'title'       =>__('Where the location woeid','smooththemes'),
        'default'=>'y',
        'type'=>'text',
        'desc'        =>'You can find woeid  at <a target="_blank" href="http://woeid.rosselliot.co.nz/lookup/">http://woeid.rosselliot.co.nz/lookup/</a>',
        'desc_bottom' =>''
     )
     ,
     
    
    
    array(
        'name'        =>'wheather_unit',
        'title'       =>__('Temperature Unit','smooththemes'),
        'options'=>array('f'=>__('Fahrenheit'),'c'=>__('Celsius')),
        'default'=>'f',
        'type'=>'radio',
        'desc'        =>'',
        'desc_bottom' =>''
     )
     
);


$posttype_tab = array(
  array(
        'name'        =>'post_room',
        'title'       =>__('Change room slug','smooththemes'),
        'default'=>'',
        'type'=>'text',
        'desc'        =>'Change/Rewrite the permalink when you use permalink as /%postname%/ ',
        'desc_bottom' =>''
     ),
    array(
        'name'        =>'post_event',
        'title'       =>__('Change event slug','smooththemes'),
        'default'=>'',
        'type'=>'text',
        'desc'        =>'Change/Rewrite the permalink when you use permalink as /%postname%/ ',
        'desc_bottom' =>''
     )
     
    
);




// ========================== Setup Load Panel ========================== \\

$tabs_settings =  new Smooththemes_tabs_settings();

// General Setting
$tabs_settings->add_tab('general',__('General Setings','smooththemes'),$general_tab,'icon-cog');
    $tabs_settings->add_tab('general_page',__('Page Settings','smooththemes'),$general_tab_page,'icon-caret-right','general');
    $tabs_settings->add_tab('general_logo',__('Logo','smooththemes'),$general_tab_logo,'icon-caret-right','general');
    $tabs_settings->add_tab('general_favicon',__('Favicon','smooththemes'),$general_tab_favicon,'icon-caret-right','general');
    $tabs_settings->add_tab('general_sidebar',__('Custom Sidebars','smooththemes'),$sidebar_tab,'icon-caret-right','general');

// Font Style Setting
$tabs_settings->add_tab('fonts',__('Font Style','smooththemes'),'','icon-font');
    $tabs_settings->add_tab('fonts_body',__('Body Font','smooththemes'),$font_body,'icon-caret-right','fonts');
    $tabs_settings->add_tab('fonts_heading',__('Heading Font','smooththemes'),$font_heading,'icon-caret-right','fonts');

// Color Setting
$tabs_settings->add_tab('elements_color',__('Elements Color','smooththemes'),$elements_color,'icon-magic');
   // $tabs_settings->add_tab('body_predefined_colors',__('Global Skin','smooththemes'),$global_skin_tab,'icon-caret-right','elements_color');
   // $tabs_settings->add_tab('link_btn',__('Link & Button','smooththemes'),$links_buttons,'icon-caret-right','elements_color');
   // $tabs_settings->add_tab('head_bg',__('Header','smooththemes'),$header_bg_tab,'icon-caret-right','elements_color');



// Overall Elements
$tabs_settings->add_tab('overall_elements',__('Overall Elements','smooththemes'),'','icon-cogs');
     
    $tabs_settings->add_tab('blog_post',__('Blog posts','smooththemes'),$blog_tab,'icon-caret-right','overall_elements'); 
    $tabs_settings->add_tab('single_setting',__('Single Post Elements','smooththemes'),$oe_single_post,'icon-caret-right','overall_elements');
    //$tabs_settings->add_tab('social',__('Social','smooththemes'),$oe_social,'icon-caret-right','overall_elements');
    $tabs_settings->add_tab('footer_copyright',__('Footer Copyright','smooththemes'),$oe_footer_copyright,'icon-caret-right','overall_elements');
    
   
  //  $tabs_settings->add_tab('room',__('Single Room','smooththemes'),$room_tab,'icon-caret-right','overall_elements');  
   // $tabs_settings->add_tab('subcribe',__('Subcribe','smooththemes'),$subcribe_tab,'icon-caret-right','overall_elements');  
    $tabs_settings->add_tab('posttype_slug',__('Links Slug','smooththemes'),$posttype_tab,'icon-caret-right','overall_elements'); 
    
    $tabs_settings->add_tab('wheather',__('Wheather','smooththemes'),$wheather_tab,'icon-caret-right','overall_elements');  
     


// Slider Setting
$tabs_settings->add_tab('slider',__('Sliders','smooththemes'),array(),'icon-exchange');
    $tabs_settings->add_tab('flexslider',__('FlexSlider','smooththemes'),$tab_flexslider,'icon-caret-right','slider');
    $tabs_settings->add_tab('screen_slider',__('Full Screen','smooththemes'),$tab_sceen_slider,'icon-caret-right','slider');
   

// Ads Management
//$tabs_settings->add_tab('ads',__('Ads Management','smooththemes'),array(),'icon-cogs');
  //  $tabs_settings->add_tab('ads_manage',__('Ads Management','smooththemes'),$ads_tab,'icon-caret-right','ads');

// for header and footer code
$tabs_settings->add_tab('tracking_code',__('Tracking code','smooththemes'),$tracking_code,'icon-cogs');




function st_build_google_font_options_url($font){
     if(empty($font['family']) || $font['family']=='' ){
                continue;
     }
     
     $variants = '';
     if(isset($font['variants']) && count($font['variants'])){
          $variants =  join(',',$font['variants']);
     }
     $subsets= '';
     if(isset($font['subsets'])&& count($font['subsets'])){
        $subsets = join(',',$font['subsets']);
     }
     
    $url = 'http://fonts.googleapis.com/css?family='.urlencode($font['family']); 
    if($variants!=''){
        $url.=':'.urlencode($variants);
    }
    if($subsets!=''){
          $url  .= '&subset='.urlencode($subsets);
    }

    return $url;
}



// Load Google Webfonts
function st_google_font_to_options(){
     if(!function_exists('st_get_google_fonts_array')){
        if(is_file(dirname(__FILE__).'/google-fonts.php')){
             include(dirname(__FILE__).'/google-fonts.php');
        } 
    }
    
     if(!function_exists('st_get_google_fonts_array')){ 
       
            return array();
      }
      $google_fonts = st_get_google_fonts_array();
      
     // echo count($google_fonts);
      $font_options = array();
      foreach($google_fonts as $k=> $font){
            if(empty($font['family']) || $font['family']=='' ){
                continue;
            }
            
            $font_options[$font['family']] = st_build_google_font_options_url($font);
      }
      return $font_options;
    
}





