<?php
/**
 * @author Sa Truong - http://www.smooththemes.com
 * @copyright 2012
 */

function st_js_encode_object($l10n){
    foreach ( (array) $l10n as $key => $value ) {
			if ( !is_scalar($value) )
				continue;

			$l10n[$key] = html_entity_decode( (string) $value, ENT_QUOTES, 'UTF-8');
		}
        return  $l10n;
}

/**
 * Custom wp_localize_script;
 */ 
function  st_js_object_var($object_name,$l10n = array()){
     return  "\n var $object_name = " . json_encode($l10n) . '; '."\n";
}




/**
 * return data type
 */ 
function st_data_type($var,$type='string'){
    switch(strtolower($type)){
        case 'bool': case 'boolean':
            $var = strtolower($var);
            if($var=='n' || $var == '' || $var ===0 || $var==false || $var=='false'){
                return  'false' ;
            }else{
                return 'true';
            }
        break;
        case 'float':
            return floatval($var);
        break;
        case  'array_join':
            return join(',',$var);
        break;
        case 'int':
             return intval($var); 
        break;
        default: 
            return $var; 
    }
    $var;
}

function st_flex_js_settings(){
    
    $settings =  array(
            'animation'=>'string',
            'animationLoop'=>'bool',
            'animationDuration'=>'int',
            'slideshow'=>'bool',
            'slideshowSpeed'=>'int',
            'animationSpeed'=>'int',
            'pauseOnAction'=>'bool',
            'pauseOnHover'=>'bool', 
            'controlNav'=>'bool',
            'randomize'=>'bool',
            'directionNav'=>'bool'
    );
    $js_options = array();
    foreach($settings as $name => $type){
         $v = st_get_setting('flex_'.$name,'');
         if(isset($v) && !empty($v)){
             $js_options[$name] =  st_data_type($v,$type);
         }
    }
    
   // $js_options['nextText'] ='<i class="icon-chevron-right"></i>';
   // $js_options['prevText'] ='<i class="icon-chevron-left"></i>';
    
    return $js_options;
    
}




#-----------------------------------------------------------------
# Enqueue Scripts & Styles
#-----------------------------------------------------------------
if(!function_exists('st_enqueue_scripts')){
    function st_enqueue_scripts(){
        if(!is_admin()){
            
            $is_full_page = false;
            if(is_page()){
                global $post;
                $st_page_builder = get_page_builder_options($post->ID);
                if(isset($st_page_builder['page_template'])  && $st_page_builder['page_template'] =='full'){
                      $is_full_page = true;
                }
            }
            

            //wp_enqueue_script("jquery",st_js('jquery.js'));
            wp_enqueue_script("jquery");
            wp_enqueue_script( 'cookies', st_js('cookies.js'), array('jquery'),'1.0', true);
           // wp_enqueue_script( 'jquery-easing', st_js('jquery.easing.min.js'), array('jquery'),'1.3', true);
            wp_enqueue_script( 'tools', st_js('jquery.tools.js'), array('jquery'),'1.2.7', true);
            wp_enqueue_script( 'validate', st_js('jquery.validate.js'), array('jquery'),'1.9.0', true);
            wp_enqueue_script( 'plug_ins', st_js('plug_ins.js'), array('jquery'),'1.0', true);
            wp_enqueue_script( 'maps', 'http://maps.googleapis.com/maps/api/js?sensor=true', array('jquery'),'', true);
            //wp_enqueue_script( 'google_map', st_js('google_map.js'), array('jquery'),ST_VERSION, true);
            
            if($is_full_page){
                wp_enqueue_script( 'supersized', st_js('supersized.3.2.6.min.js'), array('jquery'),'3.2.6', true);
                wp_enqueue_script( 'supersized-theme', st_asset('theme/supersized.shutter.min.js'), array('jquery'),'3.2.6', true);
                
            }
            
            wp_enqueue_script( 'simpleWeather', st_js('jquery.simpleWeather.js'), array('jquery'),'2.2.0', true);

            wp_enqueue_script( 'flexi_slider', st_js('flexi_slider.js'), array('jquery'),'1.8', true);
            wp_enqueue_script( 'fitvids', st_js('jquery.fitvids.js'), array('jquery'),'1.0', true);
            
           // wp_enqueue_script( 'twitter', st_js('twitter.js'), array('jquery'),'1.0', true);

            wp_enqueue_script( 'functions', st_js('functions.js'), array('jquery'),'1.0', true);
            wp_enqueue_script( 'shortcodes', st_js('shortcodes.js'), array('jquery'),'1.0', true);
            
            wp_enqueue_script( 'custom', st_js('custom.js'), array('jquery'),'1.0', true);
    
        	if ( is_singular() && get_option( 'thread_comments' ) ){
  	     	   wp_enqueue_script( 'comment-reply' );
        	}

        }
    }
}
add_action('wp_print_scripts','st_enqueue_scripts');


function st_print_sltyles(){
     $is_full_page = false;
            if(is_page()){
                global $post;
                $st_page_builder = get_page_builder_options($post->ID);
                if(isset($st_page_builder['page_template'])  && $st_page_builder['page_template'] =='full'){
                      $is_full_page = true;
                }
            }
            
            //Register styles
            
             wp_enqueue_style('font-awesome',st_css('font-awesome.min.css'));
            wp_enqueue_style('resopsive',st_css('resopsive.css'));
            wp_enqueue_style('st_style', get_bloginfo( 'stylesheet_url' ),false, '', 'screen');
            wp_enqueue_style('base', st_css('base.css'),false, '', 'screen');
           
            wp_enqueue_style('menu',st_css('menu.css'));
            wp_enqueue_style('flexslider',st_css('flexslider.css'));
            wp_enqueue_style('layout',st_css('layout.css'));
           
            wp_enqueue_style('calendar',st_css('calendar.css'));
            
            if($is_full_page){
                wp_enqueue_style('supersized',st_css('supersized.css'));
                wp_enqueue_style('supersized-theme',st_asset('theme/supersized.shutter.css'));
            }
            
            if(st_is_woocommerce()){
                wp_enqueue_style('st_wc',ST_WC_TEMPLATE_URL.'assets/css/woocommerce.css');
            }
            wp_enqueue_style('prettyPhoto',st_js('prettyPhoto/css/prettyPhoto.css'));
 
}
add_action('wp_print_styles','st_print_sltyles');


function st_user_custom_code(){
?>
<!--[if IE 8]><link href="<?php st_css('ie8.css',true); ?>" rel="stylesheet" type="text/css" /><![endif]-->
<link href="<?php echo ST_THEME_URL .'custom.css'; ?>" rel="stylesheet" type="text/css" />
<?php
}

add_action('wp_head','st_user_custom_code',50);



function st_localize_script(){
    
   $wheather  = array();
   $wheather['show'] = st_get_setting('wheather_show','y');
   $wheather['woeid'] = st_get_setting('wheather_woeid','');
   $wheather['unit'] = st_get_setting('wheather_unit','f');
   if($wheather['woeid']==''){
       $wheather['show'] = 'n';
   }
   
    
?>
<script type="text/javascript">
//<![CDATA[
<?php 
    echo  st_js_object_var('ajax_url',admin_url('admin-ajax.php')); 
    echo  st_js_object_var('st_wheather',$wheather); 
    echo  st_js_object_var('FS',st_flex_js_settings());
?>
//]]>
</script>
<?php
}
add_action('wp_head','st_localize_script',1);

function st_footer_validate_msg(){
?>
<script type="text/javascript">
//<![CDATA[
 var validateMsg = {
        required: <?php echo json_encode(__('This field is required.','smooththemes')); ?>,
        remote: <?php echo json_encode(__('Please fix this field.','smooththemes')); ?>,
        email: <?php echo json_encode(__('Please enter a valid email address.','smooththemes')); ?>,
        url: <?php echo json_encode(__('Please enter a valid URL.','smooththemes')); ?>,
        date: <?php echo json_encode(__('Please enter a valid date.','smooththemes')); ?>,
        dateISO: <?php echo json_encode(__('Please enter a valid date (ISO).','smooththemes')); ?>,
        number: <?php echo json_encode(__('Please enter a valid number.','smooththemes')); ?>,
        digits: <?php echo json_encode(__('Please enter only digits.','smooththemes')); ?>,
        creditcard: <?php echo json_encode(__('Please enter a valid credit card number.','smooththemes')); ?>,
        equalTo:<?php  echo json_encode(__('Please enter the same value again.','smooththemes')); ?>,
        accept: <?php echo json_encode(__('Please enter a value with a valid extension.','smooththemes')); ?>,
        maxlength: jQuery.validator.format(<?php echo json_encode(__('Please enter no more than {0} characters.','smooththemes')); ?>),
        minlength: jQuery.validator.format(<?php echo json_encode(__('Please enter at least {0} characters.','smooththemes')); ?>),
        rangelength: jQuery.validator.format(<?php echo json_encode(__('Please enter a value between {0} and {1} characters long.','smooththemes')); ?>),
        range: jQuery.validator.format(<?php echo json_encode(__('Please enter a value between {0} and {1}.','smooththemes')); ?>),
        max: jQuery.validator.format(<?php echo json_encode(__('Please enter a value less than or equal to {0}.','smooththemes')); ?>),
        min: jQuery.validator.format(<?php echo json_encode(__('Please enter a value greater than or equal to {0}.','smooththemes')); ?>)
}
//]]>
</script>
<?php
}
 add_action('wp_footer','st_footer_validate_msg',89);



