<form role="search" method="get"  class="searchform" action="<?php echo home_url( '/' ); ?>">
   
        <input type="text" value="" name="s" id="s" class="s" placeholder="<?php echo esc_attr('Search..','smooththemes'); ?>" />
        <input type="submit" class="searchsubmit" value="<?php _e('Search','smooththemes'); ?>" />
        <div class="clear"></div>
    
</form>