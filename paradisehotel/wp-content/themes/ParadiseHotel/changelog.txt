Version 1.4
 - Update admin options use AJAX to save.
    Changed file:
            st-framework/admin/admin-functions.php
            st-framework/admin/admin-int.php
            st-framework/admin/admin-interface.php
            st-framework/admin/css/admin-style.css
            st-framework/st-load.php
    - Update WC template
        Changed file:
            woocommerce/single-product/add-to-cart/grouped.php

    - Fixed shortcodes issue in WP editor with WordPress 3.9
        Changed file:
            st-framework/admin/editor/edittor_plugin.js
            st-framework/admin/editor/tinymce.js


Version 1.3
    - Variable products do not work
        Changed file: /woocommerce/single-product/add-to-cart/variable.php
    - The message does not appear
        Add new file:   /woocommerce/notices/error.php
                        /woocommerce/notices/notice.php
                        /woocommerce/notices/success.php

Ver 1.2
- Fixed slider full width not fit
    Changed file: /style.css
    Changed file: /custom.js
- Fixed bug missing translate text
    Changed file: templates/forms/availability.php
- New admin option interface
    Changed file: /st-framework/admin/css/admin-style.css
    Changed file: /st-framework/admin/admin-interface.php
    Changed file: /st-framework/admin/images/logo.jpg

Ver 1.1
- Fixed  Email didn't get the reservation date
    Changed file: st-framework/lib/st-ajax.php
- Fixed Twitter Widget
    Changed file assets/js/custom.js
    Removed file assets/js/twitter.js
    Changed file st-framework/lib/widgets/twitter.php
    Changed file st-framework/settings/js-and-css.php
- Fixed some Css bugs
    Changed file: assets/css/base.css
				  layout.css
				  theme/supersized.shutter.css
                           