<!DOCTYPE html>
<!--[if IE 7 ]><html class="ie ie7" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html <?php language_attributes(); ?>> <!--<![endif]-->
<head>

<!-- Basic Page Needs -->
<meta charset="<?php bloginfo('charset'); ?>">
<title><?php
    /*
     * Print the <title> tag based on what is being viewed.
     */
    global $page, $paged;

    wp_title( '|', true, 'right' );

    // Add the blog name.
    bloginfo( 'name' );

    // Add the blog description for the home/front page.
    $site_description = get_bloginfo( 'description', 'display' );
    if ( $site_description && ( is_home() || is_front_page() ) )
        echo " | $site_description";

    // Add a page number if necessary:
    if ( $paged >= 2 || $page >= 2 )
        echo ' | ' . sprintf( __( 'Page %s', 'smooththemes' ), max( $paged, $page ) );

    ?></title>

<!-- Favicons-->
<?php if(''!=st_get_setting('site_favicon','')): ?>
<link rel="shortcut icon" href="<?php echo esc_attr(st_get_setting('site_favicon')); ?>" />
<?php endif; ?>

<!-- Mobile Specific Metas -->
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<!--[if lt IE 9]>
<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->

<?php
wp_head();
?>
<body <?php body_class(); ?>>
<div  id="site-wrapper" class="<?php echo st_get_setting("page_full_boxed",'f')!='b' ? 'fullwidth' : 'boxed'; ?>">
<header id="header">
<div class="container">
  <div class="row b0 p-r">

        <div class="three column b0 logo-wrap ">
            <div id="logo">
                <a href="<?php echo site_url('/'); ?>" title="<?php echo esc_attr(get_bloginfo('name')); ?>" class="<?php echo (st_get_setting("site_logo")!='' ) ? 'has-img' :'no-img'; ?>">
                    <span class="logo-border">
                        <?php if(st_get_setting("site_logo")!=''){ ?>
                        <img src="<?php echo st_get_setting("site_logo"); ?>" />
                        <?php }else{ ?>
                          <span class="text-only"><?php  bloginfo('name'); ?></span>
                        <?php } ?>
                        
                    </span>
                </a>
             </div>
             
            
        
        </div>
        <div class="nine column b0">
                
                <div class="row m0">
                    
                    <div class="twelve column b0 p0 top-right-weala<?php echo (st_get_setting('wheather_show','y')=='n' && !st_is_wpml()) ? ' nothing-insise' : ' waw'; ?>">
                        
                        <?php
                        /**
                         * Language switcher if WPML is installed
                         * 
                         */
                         
                         if(st_is_wpml()){
                             
                           $langs =  icl_get_languages('skip_missing=0&orderby=name&order=ASC&link_empty_to='.home_url()) ;
                             
                        ?>
                		<ul id="lang">
                		    <?php foreach($langs as $l){
                                 echo '<li><a href="'.$l['url'].'" title="'.$l['native_name'].'">'.$l['language_code'].'</a></li>';
                		    } ?>
                		</ul>
                		<?php }else{
                		     
                		} ?>
                		
                		
                		
                		
                		<?php
                		/**
                         * Display wheather template
                         * */
                		 if(st_get_setting('wheather_show','y')=='y'){ ?>
                    		<div id="wxWrap">
                    		    <span id="wxIntro"></span>
                    		    <span id="wxIcon2"></span><span id="wxTemp"></span>
                    		</div><!-- weather -->
                	    <?php } ?>
                	
                	</div>
                	
                	
                    <!-- start main nav -->
                	<div class="twelve column b0  p0">
                		<nav>
                    		<ul id="main-nav" class="sf-menu">
                    		<?php 
                                $defaults = array(
                                	'theme_location'  => 'Primary_Navigation',
                                	'container'       => false,
                                    'container_class' => false,
                                    'items_wrap'=>'%3$s',
                                	'echo'            => true
                                );
                               wp_nav_menu( $defaults );
                                ?>
                    		</ul>
                		</nav><!-- end main nav -->
                       
                	</div><!-- end twelve -->
                    
                 <div class="clear"></div>
                </div>
        
    </div>

     <div class="clear"></div>
    </div>
</div><!-- end container -->
</header><!-- end header-->



