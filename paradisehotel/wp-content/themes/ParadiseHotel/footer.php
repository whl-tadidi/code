<footer>
    <div class="container">
    <div class="row p0  b0">
    
   
        <nav class="six columns b0">
        <ul id="nav-footer">
           <?php 
                $defaults = array(
                	'theme_location'  => 'Footer_Navigation',
                	'container'       => false,
                    'container_class' => false,
                    'items_wrap'=>'%3$s',
                	'echo'            => true
                );
               wp_nav_menu( $defaults );
               ?>
        </ul>
        </nav>
    <div class="six columns copy b0"><?php echo stripslashes(st_get_setting("footer_copyright")); ?></div>
    <div class="clear "></div>
     </div>
    </div>
</footer><!-- footer  -->

<?php wp_footer(); ?>

</div><!-- site-wrapper -->
</body>
</html>