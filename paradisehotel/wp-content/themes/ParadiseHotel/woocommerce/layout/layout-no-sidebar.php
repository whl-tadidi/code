<?php 
/**
 * FULL layout no sidebar
 */ 
?>


<div class="main-outer-wrapper woocommerce-layout">
   <div class="main-wrapper container">
      <div class="row no-sidebar">
        <?php 
       /**
         * @hooked st_page_title_tempalte();
         */ 
        do_action('st_top_page_template'); 
        ?>

        <div class="content-wrapper twelve  columns b0">
         <?php
             woocommerce_content();
          ?>
         </div>
        
         
        <div class="clear"></div>
         <?php do_action('st_bottom_page_template'); ?>
        <div class="clear"></div>

        </div>
    </div><!-- main-wrapper  -->
     <?php do_action('st_bottom_main_wrapper'); ?>
</div><!-- END .main-outer-wrapper  -->



